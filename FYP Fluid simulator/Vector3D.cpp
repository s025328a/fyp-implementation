#include "Vector3D.h"

Vector3D::Vector3D()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}

Vector3D::Vector3D(float _x, float _y, float _z)
{
	x = _x;
	y = _y;
	z = _z;
}

float Vector3D::Length()
{
	return sqrtf((x * x) + (y * y) + (z * z));
}

float Vector3D::LengthSquared()
{
	return (x * x) + (y * y) + (z * z);
}

Vector3D Vector3D::UnitVector()
{
	float length = Length();

	if (length > 0.0f)
	{
		return Vector3D(x / length, y / length, z / length);
	}
	return Vector3D(0, 0, 0);
}
