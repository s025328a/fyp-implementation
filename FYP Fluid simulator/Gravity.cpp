#include "Gravity.h"

Gravity::Gravity(Vector3D acceleration)
{
	_acceleration = acceleration;
}

Vector3D Gravity::Calculate(Particle* particle)
{
	return particle->GetMass() * _acceleration;
}