#ifndef VISCOSITY_KERNEL_H
#define VISCOSITY_KERNEL_H

#include "ISmoothingKernel.h"

class ViscosityKernel : public ISmoothingKernel
{
private:
	float _laplacian;
	float _h2;

public:
	ViscosityKernel(float h);

	float Value(Vector3D rij, float h){ return 0.0f; }
	Vector3D Gradient(Vector3D rij, float h){ return Vector3D(0, 0, 0); }
	float Laplacian(Vector3D rij, float h);

	void SetValues();
	void SetKernelRadius(float h);
};

#endif