#include "Fluid.h"

Fluid::Fluid()
{

}

void Fluid::Initialize(float mass, float rho0, float mu, float k)
{
	_mass = mass;
	_rho0 = rho0;
	_mu = mu;
	_k = k;
}

void Fluid::AddForce(IForce* force)
{
	_forces.push_back(force);
}

void Fluid::AddScalar(IScalar* scalar)
{
	_scalars.push_back(scalar);
}