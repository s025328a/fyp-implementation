#ifndef POLY6_KERNEL_H
#define POLY6_KERNEL_H

#include "ISmoothingKernel.h"
#include "Vector3D.h"
#include <math.h>

class Poly6Kernel : public ISmoothingKernel
{
private:
	float _value;
	float _gradient;
	float _h2;

public:
	//Poly6Kernel();
	//~Poly6Kernel();
	Poly6Kernel(float h, float scale);
	
	float Value(Vector3D rij, float h);
	Vector3D Gradient(Vector3D rij, float h);

	void SetValues();
	void SetKernelRadius(float h);
};

#endif