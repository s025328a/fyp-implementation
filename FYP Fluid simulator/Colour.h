#ifndef COLOUR_H
#define COLOUR_H

#include "IScalar.h"
#include "ISmoothingKernel.h"
#include "Particle.h"

class Colour// : public IScalar
{
private:
	ISmoothingKernel* _kernel;

public:
	Colour();
	Colour(ISmoothingKernel* kernel){ _kernel = kernel; }

	float Calculate(Particle* particle);
};

#endif