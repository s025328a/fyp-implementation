#include "ViscosityMuller.h"

ViscosityMuller::ViscosityMuller(ISmoothingKernel* kernel)
{
	_kernel = kernel;
}

Vector3D ViscosityMuller::Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j)
{
	Vector3D force = _kernel->Laplacian(i->GetPosition() - j->GetPosition(), core) * (j->GetMass() * (j->GetVelocity() - i->GetVelocity()) / j->GetDensity());
		
	Vector3D visc = i->GetFluid()->GetViscosityCoefficient() *(i->GetMass() / i->GetDensity()) * force;

	if (visc.x != 0 && visc.y != 0 && visc.z != 0)
	{
		int a = 1;
	}
	return visc;
}