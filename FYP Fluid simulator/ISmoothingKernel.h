#ifndef ISMOOTHINGKERNEL_H
#define ISMOOTHINGKERNEL_H

#include "Vector3D.h"

#define PI 3.14159265f
#define core 0.04f

class ISmoothingKernel
{
protected:
	float _h;
	float _scale = 1.0f;
	float value;
	float gradient;
	float laplacian;

public:
	//void SetCoreRadius(float h){ _h = h; }
	void SetScaleY(float yScale){ _scale = yScale; }

	virtual float Value(Vector3D rij, float h) = 0;
	virtual Vector3D Gradient(Vector3D rij, float h) = 0;

	virtual void SetKernelRadius(float h)
	{
		_h = h;
	}

	virtual float Laplacian(Vector3D rij, float h)
	{
		return 0;
	}

	float Power(float val, int power)
	{
		for (int i = 2; i <= power; ++i)
		{
			val *= val;
		}
		return val;
	}
};

#endif