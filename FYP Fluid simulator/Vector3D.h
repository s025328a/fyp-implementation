#ifndef VECTOR3D_H
#define VECTOR3D_H

#include <math.h>

//Vector3D operator * (const Vector3D &v, float scalar);

struct IndexRange
{
	int start;
	int end;
};

class Int3
{
public:
	int x, y, z;

	Int3(){}
	Int3(int i, int j, int k)
	{
		x = i;
		y = j;
		z = k;
	}

	friend bool operator== (const Int3& i1, const Int3& i2)
	{
		return i1.x == i2.x && i1.y == i2.y && i1.z == i2.z;
	}
};

class Vector3D
{
public:
	float x, y, z;

	Vector3D();
	//~Vector3D();

	Vector3D(float _x, float _y, float _z);

	float Length();
	float LengthSquared();

	Vector3D UnitVector();

	Int3 Floor()
	{
		return Int3((int)floor(x), (int)floor(y), (int)floor(z));
	}

	// operators
	friend Vector3D operator* (float scalar, const Vector3D& v)
	{
		return Vector3D(v.x * scalar, v.y * scalar, v.z * scalar);
	}

	friend Vector3D operator/ (const Vector3D& v, float scalar)
	{
		return Vector3D(v.x / scalar, v.y / scalar, v.z / scalar);
	}

	friend Vector3D operator+ (const Vector3D& v1, const Vector3D& v2)
	{
		return Vector3D(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
	}

	friend Vector3D operator- (const Vector3D& v1, const Vector3D& v2)
	{
		return Vector3D(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
	}

	friend bool operator> (const Vector3D& v1, const Vector3D& v2)
	{
		return v1.x > v2.x && v1.y > v2.y && v1.z > v2.z;
	}

	friend bool operator< (const Vector3D& v1, const Vector3D& v2)
	{
		return v1.x < v2.x && v1.y < v2.y && v1.z < v2.z;
	}

	friend bool operator<= (const Vector3D& v1, const Vector3D& v2)
	{
		return v1.x <= v2.x && v1.y <= v2.y && v1.z <= v2.z;
	}
};

#endif