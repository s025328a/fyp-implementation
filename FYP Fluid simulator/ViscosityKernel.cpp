#include "ViscosityKernel.h"

ViscosityKernel::ViscosityKernel(float h)
{
	_h = h;
	SetValues();
}

void ViscosityKernel::SetValues()
{
	_h2 = _h * _h;
	_laplacian = _scale * (45.0f / (PI * pow(_h, 6)));
}

void ViscosityKernel::SetKernelRadius(float h)
{
	_h = h;
	SetValues();
}

float ViscosityKernel::Laplacian(Vector3D rij, float h)
{
	if (rij.LengthSquared() < _h2)
	{
		float r = rij.Length();
		//return result = _scale * (45.0f / (PI * pow(h, 6))) * (h - r);
		return (_h - r) * _laplacian;
	}
	return 0.0f;
}