#ifndef PARTICLE_H
#define PARTICLE_H

#include "Vector3D.h"
#include "Fluid.h"
#include "GridCell.h"
#include "IGrid.h"
#include <vector>
#include <memory>

// forward declaration
class GridCell;

class Particle
{
private:
	uint64_t _index;

	float _rho; // density
	float _default_rho = 0.0f;
	float _p; // pressure
	float _c; // colour
	Vector3D _n; // colour gradient
	float _mass;

	Vector3D _position;
	Vector3D _start_pos;
	Vector3D _velocity;
	Vector3D _acc;
	
	Vector3D _net_force;

	Fluid* _fluid;
	shared_ptr<GridCell> _grid_cell;
	IGrid* _grid;

	vector<shared_ptr<Particle>> _neighbours;
	vector<shared_ptr<Particle>> _boundary_neighbours;

	shared_ptr<Particle> _root_neighbour;

	vector< vector<shared_ptr<Particle>> > neighbours;
	
	void InitDefault();

	shared_ptr<Particle> _next = nullptr;

	//vector<uint64_t> _neighbour_indices;
	vector<IndexRange> _neighbour_indices;
	vector<IndexRange> _boundary_neighbour_indices;

	float _h;

	XMFLOAT4 _colour = XMFLOAT4(-1.0f, -1.0f, -1.0f, -1.0f);

	void CalculateZIndex();


public:
	Particle();
	~Particle();

	int _neighbour_count = 0;
	bool _original = true;

	friend bool operator < (Particle& p1, Particle& p2)
	{
		return p1.GetIndex() < p2.GetIndex();
	}
	
	Particle(Vector3D position, Fluid* fluid, int index, float h);
	
	Vector3D GetPosition(){ return _position; }
	Vector3D GetVelocity(){ return _velocity; }

	void SetVelocity(Vector3D velocity){ _velocity = velocity; }
	void SetPosition(Vector3D position){ _position = position; }

	void SetGrid(IGrid*& grid){ _grid = grid; }

	void CalculateDensity();
	void CalculatePressure();

	void UpdateNeighbours();
	void UpdateForces();
	void UpdatePosition(float t);

	XMFLOAT4 GetColour()
	{ 
		if (_colour.x >= 0.0f)
		{
			return _colour;
		}
		return _fluid->GetColour(); 
	}

	void SetColour(float r, float g, float b, float a){ _colour = XMFLOAT4(r, g, b, a); }

	void SetTimestep(float t, float& min);

	void ResetScalars()
	{
		_rho = 0.0f;
		_p = 0.0f;
		_c = 0.0f;
		_n = Vector3D(0, 0, 0);
	}

	void SetIndex(uint64_t index){ _index = index; }
	uint64_t GetIndex(){ return _index; }

	float GetMass(){ return _mass; }

	float GetDensity()
	{
		if (_rho > 0.0f)
		{
			return _rho;
		}
		return _default_rho;
	}

	float GetPressure(){ return _p; }
	Vector3D GetColourGradient(){ return _n; }

	void AddDensity(float rho){ _rho = _rho + rho; }
	void AddColourGradient(Vector3D normal){ _n = _n + normal; }

	vector<shared_ptr<Particle>>& GetNeighbours(){ return _neighbours; }
	vector<shared_ptr<Particle>>& GetBoundaryNeighbours(){ return _boundary_neighbours; }
	vector<vector<shared_ptr<Particle>>> GetNeighbourList(){ return neighbours; }
	//vector<shared_ptr<GridCell>> GetNeighbourCells(){ return _grid_cell->GetNeighbourCells; }

	void RecalculateIndex();

	//vector<uint64_t> GetNeighbourIndices(){ return _neighbour_indices; }
	vector<IndexRange> GetNeighbourIndices(){ return _neighbour_indices; }
	vector<IndexRange>& GetBoundaryNeighbourIndices(){ return _boundary_neighbour_indices; }


	void SetDensity(float rho){ _rho = _default_rho + rho; }
	void SetDefaultDensity(float rho){ _default_rho = rho; }
	void SetPressure(float p){ _p = p; }
	void SetColourGradient(Vector3D n){ _n = n; }

	void SetNetForce(Vector3D net){ _net_force = net; }

	void SetGridCell(shared_ptr<GridCell>& cell){ _grid_cell = cell; }

	Fluid*& GetFluid(){ return _fluid; }

	shared_ptr<Particle>& Next(){ return _next; }
	void SetNext(shared_ptr<Particle> next){ _next = next; }

	void Reset();

	void CleanUp();
};

#endif