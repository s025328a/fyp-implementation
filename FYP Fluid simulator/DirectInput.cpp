#include "DirectInput.h"

directInput::directInput()
{
	mouseInput = NULL;
	keyboardInput = NULL;
	inputDevice = NULL;
}

directInput::~directInput()
{
	
}

void directInput::cleanUpInput()
{
	if (mouseInput) 
	{
		mouseInput->Unacquire();
		mouseInput->Release();
		mouseInput = 0;
	}

	if (keyboardInput) 
	{
		keyboardInput->Unacquire();
		keyboardInput->Release();
		keyboardInput = 0;
	}

	if (inputDevice) inputDevice->Release();
}

HRESULT directInput::initDirectInput(HINSTANCE hInst, HWND hWnd)
{
	HRESULT hr;

	hr = DirectInput8Create(hInst,
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(void**)&inputDevice,
		NULL);

	if (FAILED(hr)) return hr;

	hr = inputDevice->CreateDevice(GUID_SysMouse,
		&mouseInput,
		NULL);

	if (FAILED(hr)) return hr;

	hr = inputDevice->CreateDevice(GUID_SysKeyboard,
		&keyboardInput,
		NULL);

	if (FAILED(hr)) return hr;
		
	hr = keyboardInput->SetDataFormat(&c_dfDIKeyboard);
	hr = keyboardInput->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	if (FAILED(hr)) return hr;

	hr = mouseInput->SetDataFormat(&c_dfDIMouse);
	hr = mouseInput->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);

	if (FAILED(hr)) return hr;

	rightDown = false;
	leftDown = false;

	return S_OK;
}

bool directInput::getRightClick()
{
	return rightDown;
}

bool directInput::getLeftClick()
{
	return leftDown;
}

float directInput::getXChange()
{
	return xChange;
}

float directInput::getYChange()
{
	return yChange;
}

void directInput::updateMouseInput()
{
	DIMOUSESTATE mouseState;

	mouseInput->Acquire();

	mouseInput->GetDeviceState(sizeof(DIMOUSESTATE), &mouseState);

	if (mouseState.lX != lastMouseState.lX)
	{
		xChange = mouseState.lX;
	}
	else
	{
		xChange = 0;
	}

	if (mouseState.lY != lastMouseState.lY)
	{
		yChange = mouseState.lY;
	}
	else
	{
		yChange = 0;
	}

	if (mouseState.rgbButtons[0] & 0x80)
	{
		leftDown = true;
	}
	else
	{
		leftDown = false;
	}

	if (mouseState.rgbButtons[1] & 0x80)
	{
		rightDown = true;
	}
	else
	{
		rightDown = false;
	}
}

void directInput::updateKeyboardInput()
{
	
}

int directInput::checkNumKey()
{
	BYTE keyboardState[256];

	keyboardInput->Acquire();

	keyboardInput->GetDeviceState(sizeof(keyboardState),(LPVOID)&keyboardState);

	if (keyboardState[DIK_1] & 0x80)
	{
		return 1;
	}
	if (keyboardState[DIK_2] & 0x80)
	{
		return 2;
	}
	if (keyboardState[DIK_3] & 0x80)
	{
		return 3;
	}
	if (keyboardState[DIK_4] & 0x80)
	{
		return 4;
	}
	if (keyboardState[DIK_5] & 0x80)
	{
		return 5;
	}
	if (keyboardState[DIK_6] & 0x80)
	{
		return 6;
	}
	if (keyboardState[DIK_7] & 0x80)
	{
		return 7;
	}
	if (keyboardState[DIK_8] & 0x80)
	{
		return 8;
	}
	if (keyboardState[DIK_9] & 0x80)
	{
		return 9;
	}

	return 0;
}

bool directInput::checkSpace()
{
	BYTE keyboardState[256];

	keyboardInput->Acquire();

	keyboardInput->GetDeviceState(sizeof(keyboardState),(LPVOID)&keyboardState);

	if (keyboardState[DIK_SPACE] & 0x80)
	{
		return true;
	}
	return false;
}