#include "Particle.h"

Particle::Particle()
{
	InitDefault();
}

Particle::~Particle()
{

}

Particle::Particle(Vector3D position, Fluid* fluid, int index, float h)
{
	InitDefault();
	_position = position;
	_start_pos = position;
	if (fluid != nullptr)
	{
		_fluid = fluid;
		_mass = fluid->GetMass();
	}
	_index = index;
	_h = h;
}

void Particle::Reset()
{
	_position = _start_pos;
	_velocity = Vector3D(0, 0, 0);
	_acc = Vector3D(0, 0, 0);
	_net_force = Vector3D(0, 0, 0);
	_rho = 0.0f;
	_p = 0.0f;
	_n = Vector3D(0, 0, 0);

	_neighbour_indices.clear();
}

void Particle::CleanUp()
{
	if (this != nullptr)
	{
		_fluid = nullptr;
		_grid_cell = nullptr;
		_grid = nullptr;
		_neighbour_indices.clear();
	}
}

void Particle::UpdateNeighbours()
{
	if (_grid_cell == nullptr)
		return;

	// clear and recalculate neighbouring particles
	/*if (_neighbours.size() > 0)
		_neighbours.clear();*/

	//_neighbours = _grid_cell->GetNeighbourParticles();

	/*if (_boundary_neighbours.size() > 0)
		_boundary_neighbours.clear();*/

	//_boundary_neighbours = _grid_cell->GetBoundaryParticles();
	
	_neighbour_indices = _grid_cell->GetNeighbourIndices();
	_boundary_neighbour_indices = _grid_cell->GetBoundaryNeighbourIndices();

	/*vector<IScalar*>& scalars = _fluid->GetScalars();
	for (int i = 0; i < scalars.size(); ++i)
	{
		scalars[i]->Calculate(this);
	}*/
}

void Particle::UpdateForces()
{
	_net_force = Vector3D(0, 0, 0);
	for (auto &force : _fluid->GetForces())
	{
		//_net_force = _net_force + force->Calculate(this);
	}
}

void Particle::SetTimestep(float t, float& min)
{
	/*_acc = _net_force / _fluid->GetMass();
	Vector3D vel = _velocity + (t * _acc);;
	if (t * _velocity.Length() > _h)
	{

	}*/
}

void Particle::UpdatePosition(float t)
{
	// find acceleration from net force
	Vector3D acc = _net_force / _fluid->GetMass();
		
	/*if (_position.y - _h <= 0.0f)
	{
		_velocity.y = _velocity.y - (2.0f * _velocity.y * pow(_h / _position.y, 2));
	}*/

	// update velocity and position
	_velocity = _velocity + (t * acc);
	_position = _position + (t * _velocity);
	
	/*if (t * _velocity.Length() > _h)
	{
		_velocity = (_h / t) * _velocity.UnitVector();
	}*/

	/*if (_velocity.LengthSquared() > 2196324.0f)
	{
		_velocity = 1482.0f * _velocity.UnitVector();
	}*/
}

void Particle::InitDefault()
{
	_position = Vector3D(0.0f, 0.0f, 0.0f);
	_velocity = Vector3D(0.0f, 0.0f, 0.0f);
	_rho = 0.0f;
	_p = 0.0f;
}

void Particle::CalculatePressure()
{
	float k = _fluid->GetPressureStiffness();
	float rho0 = _fluid->GetRestDensity();

	float gamma = 1.0f;

	//float b = rho0 * 88.5 * 88.5 / gamma;

	_p = k * (rho0 / gamma) * (pow(_rho / rho0, gamma) - 1.0f);
}

void Particle::RecalculateIndex()
{
	// Needs sorting so Z-index is only re-calculated when particles moves cell.
	/*if (_grid_cell != nullptr && !(_grid->GetGridPosition(_position) == _grid_cell->GetCellPosition()))
	{*/
		CalculateZIndex();
	//}
}

void Particle::CalculateZIndex()
{
	Int3 grid_pos = _grid->GetGridPosition(_position);

	unsigned int x = (unsigned int)grid_pos.x;
	unsigned int y = (unsigned int)grid_pos.y;
	unsigned int z = (unsigned int)grid_pos.z;

	uint64_t answer = 0;
	for (uint64_t i = 0; i < (sizeof(uint64_t)* CHAR_BIT) / 3; ++i) {
		answer |= ((x & ((uint64_t)1 << i)) << 2 * i) | ((y & ((uint64_t)1 << i)) << (2 * i + 1)) | ((z & ((uint64_t)1 << i)) << (2 * i + 2));
	}
	_index = answer;
}