cbuffer ConstantBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;
	float4 LightVec;
	float4 Colour;
}

struct VS_INPUT
{
	float3 Pos : POSITION;
	float3 Normal : NORMAL;
};

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 Color : COLOR;
	float3 Normal : NORMAL;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
//VS_OUTPUT VS(VS_INPUT input)
//{
//	VS_OUTPUT output;
//		
//	/*output.Pos = mul(input.Pos, World);
//	output.Pos = mul(output.Pos, View);
//	output.Pos = mul(output.Pos, Projection);*/
//	output.Pos = input.Pos;
//
//	output.Color = input.Color;
//	
//	return output;
//}
//
////--------------------------------------------------------------------------------------
//// Pixel Shader
////--------------------------------------------------------------------------------------
//float4 PS(VS_OUTPUT input) : SV_Target
//{	
//	return input.Color;
//}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;
	
	output.Pos = mul(float4(input.Pos, 1.0f), World);
	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Projection);

	output.Normal = mul(input.Normal, World);

	output.Color = Colour;

	return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
	float intensity = 0.5f;
	intensity += dot(normalize(input.Normal), normalize(LightVec.xyz));

	return input.Color * intensity;    // Yellow, with Alpha = 1
}