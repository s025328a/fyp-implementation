#include "Colour.h"

float Colour::Calculate(Particle* particle)
{
	Vector3D position = particle->GetPosition();
	
	float colour = 0.0f;
	Vector3D normal = Vector3D(0, 0, 0);

	vector<shared_ptr<Particle>>& neighbours = particle->GetNeighbours();

	for (int i = 0; i < neighbours.size(); ++i)
	{
		shared_ptr<Particle> j = neighbours[i];
		while (j != nullptr)
		{
			float rhoj = j->GetDensity();
			normal = normal + j->GetMass() * _kernel->Gradient(position - j->GetPosition(), core) / rhoj;
			j = j->Next();
		}
	}

	particle->SetColourGradient(normal);
	return 0.0f;
}