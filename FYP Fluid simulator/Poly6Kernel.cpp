#include "Poly6Kernel.h"

Poly6Kernel::Poly6Kernel(float h, float scale)
{
	_scale = scale;
	_h = h;
	SetValues();
}

void Poly6Kernel::SetValues()
{
	_value = _scale * (315.0f / (64.0f * PI * pow(_h, 9)));
	_gradient = _scale * (-945.0f / (32.0f * PI * pow(_h, 9)));
	_h2 = _h * _h;
}

void Poly6Kernel::SetKernelRadius(float h)
{
	_h = h;
	SetValues();
}

float Poly6Kernel::Value(Vector3D rij, float h)
{
	float r2 = rij.LengthSquared();
	if (r2 <= _h2)
	{
		return (_h2 - r2) * (_h2 - r2) * (_h2 - r2) * _value;
	}
	return 0.0f;
}

Vector3D Poly6Kernel::Gradient(Vector3D rij, float h)
{
	if (rij.LengthSquared() <= h * h)
	{
		float r = rij.Length();
		return _scale * (-945.0f / (32.0f * PI * pow(h, 9))) * pow((h * h) - (r * r), 2) * rij;
	}
	return Vector3D(0, 0, 0);
}