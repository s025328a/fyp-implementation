#ifndef VISCOSITY_MULLER_H
#define VISCOSITY_MULLER_H

#include "IForce.h"
#include "Particle.h"

class ViscosityMuller : public IForce
{
private:
	ISmoothingKernel* _kernel;

public:
	ViscosityMuller();
	ViscosityMuller(ISmoothingKernel* kernel);

	string GetId(){ return "Viscosity"; }
	Vector3D Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j);
};

#endif