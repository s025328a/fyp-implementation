#ifndef GRAVITY_H
#define GRAVITY_H

#include "IForce.h"
#include "Particle.h"

class Gravity// : public IForce
{
	Vector3D _acceleration;

public:
	Gravity();
	Gravity(Vector3D acceleration);

	string GetId(){ return "Gravity"; }
	//virtual void SetSmoothingKernel(ISmoothingKernel* kernel) = 0;
	Vector3D Calculate(Particle* particle);
};

#endif