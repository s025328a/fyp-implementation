#ifndef SURFACE_TENSION_H
#define SURFACE_TENSION_H

#include "IForce.h"
#include "Particle.h"

class SurfaceTension : public IForce
{
private:
	ISmoothingKernel* _kernel;

public:
	SurfaceTension();
	SurfaceTension(ISmoothingKernel* kernel);

	string GetId(){ return "SurfaceTension"; }
	Vector3D Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j);
};

#endif