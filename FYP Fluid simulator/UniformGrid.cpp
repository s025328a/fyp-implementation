#include "UniformGrid.h"

UniformGrid::UniformGrid(float cell_size, Vector3D position, Vector3D grid_size)
{
	_cell_size = cell_size;
	_position = position;
	_size = grid_size;
}

void UniformGrid::Initialize()
{
	_width = (int)(_size.x / _cell_size) + 1;
	_height = (int)(_size.y / _cell_size) + 1;
	_depth = (int)(_size.z / _cell_size) + 1;

	for (int j = 0; j < _height; ++j)
	{
		for (int k = 0; k < _depth; ++k)
		{
			for (int i = 0; i < _width; ++i)
			{
				Vector3D pos = _cell_size * Vector3D(i, j, k);
				_cells.push_back(std::make_shared<GridCell>(GridCell(this, _position + pos, i, j, k)));
			}
		}
	}

	for (int j = 0; j < _height; ++j)
	{
		for (int k = 0; k < _depth; ++k)
		{
			for (int i = 0; i < _width; ++i)
			{
				vector<shared_ptr<GridCell>> cells = GetNeighbouringCells(i, j, k);
				int index = i + j * _width * _depth + _width * k;

				_cells[index]->SetNeighbourCells(cells);
				//_cell_dictionary.insert(_cells[index]->GetIndex(), _cells[index]);
				_cell_dictionary[_cells[index]->GetIndex()] = _cells[index];
			}
		}
	}
}

vector<shared_ptr<GridCell>> UniformGrid::GetNeighbouringCells(int i, int j, int k)
{
	vector<shared_ptr<GridCell>> cells;

	for (int _i = i - 1; _i <= i + 1; ++_i)
	{
		for (int _j = j - 1; _j <= j + 1; ++_j)
		{
			for (int _k = k - 1; _k <= k + 1; ++_k)
			{
				if (!(_i == i && _j == j && _k == k) &&
					_i >= 0 && _j >= 0 && _k >= 0 &&
					_i < _width && _j < _height && _k < _depth)
				{
					shared_ptr<GridCell> cell = _cells[_i + _j * _width * _depth + _width * _k];
					cells.push_back(cell);
				}
			}
		}
	}
	return cells;
}

void UniformGrid::AddParticle(shared_ptr<Particle>& p)
{
	//Vector3D pos = p->GetPosition();

	shared_ptr<GridCell>& cell = _cell_dictionary[p->GetIndex()];

	if (cell != nullptr)
		p->SetGridCell(cell);

	/*if (pos > _position && pos < _position + _size)
	{
		int i = (int)floor((pos.x - _position.x) / _cell_size);
		int j = (int)floor((pos.y - _position.y) / _cell_size);
		int k = (int)floor((pos.z - _position.z) / _cell_size);

		shared_ptr<GridCell> cell = _cells[i + _width * j + k * _width * _height];
		cell->AddParticle(p);
		p->SetGridCell(cell);
	}*/
}

void UniformGrid::AddBoundaryParticle(shared_ptr<Particle>& b)
{
	Vector3D pos = b->GetPosition();
	if (_position <= pos && pos < _position + _size)
	{
		int i = (int)floor((pos.x - _position.x) / _cell_size);
		int j = (int)floor((pos.y - _position.y) / _cell_size);
		int k = (int)floor((pos.z - _position.z) / _cell_size);

		shared_ptr<GridCell>& cell = _cells[i + j * _width * _depth + _width * k];
		//cell->AddBoundaryParticle(b);
		b->SetGridCell(cell);
	}
}

void UniformGrid::ClearParticles()
{
	for (int i = 0; i < _cells.size(); ++i)
	{
		_cells[i]->Clear();
	}

	/*for (auto &cell : _cells)
	{
		cell->Clear();
	}*/
}

Int3 UniformGrid::GetGridPosition(Vector3D position)
{
	int i = (int)floor((position.x - _position.x) / _cell_size);
	int j = (int)floor((position.y - _position.y) / _cell_size);
	int k = (int)floor((position.z - _position.z) / _cell_size);

	return Int3(i, j, k);
	//return ((position - _position) / _cell_size).Floor();
}

void UniformGrid::AddStartIndex(uint64_t index, int array_index)
{
	shared_ptr<GridCell>& cell = _cell_dictionary[index];
	if (cell != nullptr)
	{
		cell->SetStartIndex(array_index);
	}
}

void UniformGrid::AddEndIndex(uint64_t index, int array_index)
{
	shared_ptr<GridCell>& cell = _cell_dictionary[index];
	if (cell != nullptr)
	{
		cell->SetEndIndex(array_index);
	}
}

void UniformGrid::AddBoundaryStartIndex(uint64_t index, int array_index)
{
	shared_ptr<GridCell>& cell = _cell_dictionary[index];
	if (cell != nullptr)
	{
		cell->SetBoundaryStartIndex(array_index);
	}
}

void UniformGrid::AddBoundaryEndIndex(uint64_t index, int array_index)
{
	shared_ptr<GridCell>& cell = _cell_dictionary[index];
	if (cell != nullptr)
	{
		cell->SetBoundaryEndIndex(array_index);
	}
}

void UniformGrid::CalculateBoundaryNeighbours()
{
	for (int i = 0; i < _cells.size(); ++i)
	{
		_cells[i]->CalculateNeighbourBoundaryIndices();
	}
}

void UniformGrid::ClearArrayIndices()
{
	/*for (auto& cell : _cell_dictionary.)
	{
		cell->SetIndexRange(-1, -1);
	}*/

	for (int i = 0; i < _cells.size(); ++i)
	{
		_cells[i]->SetIndexRange(-1, -1);
	}
}

void UniformGrid::Reset()
{
	/*for (int i = 0; i < _cells.size(); ++i)
	{
		_cells[i]->CleanUp();
		_cells[i] = nullptr;
	}*/
	//_cells.clear();
	_cell_dictionary.clear();
	_cells.resize(0);
	//_cell_dictionary.
	//_cell_dictionary.clear();
}