#include "SplineKernel.h"

SplineKernel::SplineKernel(float h)
{
	_h = h;
	SetValues();
}

void SplineKernel::SetValues()
{
	_h2 = _h * _h;
	_value = 32.0f / (PI * pow(_h, 9));
	_temp = pow(_h, 6) / 64.0f;
}

void SplineKernel::SetKernelRadius(float h)
{
	_h = h;
	SetValues();
}

float SplineKernel::Value(Vector3D rij, float h)
{
	if (rij.LengthSquared() <= _h2)
	{
		float r = rij.Length();

		float c = ((_h - r) * (_h - r) * (_h - r)) * (r * r * r) * _value;
		//float c = pow(_h - r, 3) * pow(r, 3) * _value;
		//float c = pow(_h - r, 3) * pow(r, 3) * 32.0f / (PI * pow(h, 9));

		if (r <= (0.5f * _h))
		{
			return 2.0f * c - (_value * _temp);
		}
		else
		{
			return c;
		}
	}
	return 0.0f;
}

Vector3D SplineKernel::Gradient(Vector3D rij, float h)
{
	return Vector3D(0, 0, 0);
}