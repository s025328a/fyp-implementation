#ifndef FLUID_H
#define FLUID_H

//#include "Particle.h"
#include "IScalar.h"
#include "IForce.h"
#include <vector>
#include <directxmath.h>

using namespace std;
using namespace DirectX;

class Particle;

class Fluid
{
private:
	float _mass;
	float _rho0; // rest density;
	float _mu; // viscosity coefficient
	float _k; // pressure stiffness;

	XMFLOAT4 _colour;

	//vector<shared_ptr<Particle>> _particles;

	IScalar* _density;
	IScalar* _pressure;

	vector<IForce*> _forces;
	vector<IScalar*> _scalars;
public:
	Fluid();
	~Fluid();

	void Initialize(float mass, float rho0, float mu, float k);
	//void SetParticles(vector<shared_ptr<Particle>> particles);

	float GetMass(){ return _mass; }
	float GetPressureStiffness(){ return _k; }
	float GetRestDensity(){ return _rho0; }
	float GetViscosityCoefficient(){ return _mu; }

	void AddForce(IForce* force);
	vector<IForce*>& GetForces(){ return _forces; }

	void AddScalar(IScalar* scalar);
	vector<IScalar*>& GetScalars(){ return _scalars; }

	IScalar* GetDensityScalar(){ return _density; }
	IScalar* GetPressureScalar(){ return _pressure; }

	XMFLOAT4 GetColour(){ return _colour; }
	void SetColour(float r, float g, float b, float a){ _colour = XMFLOAT4(r, g, b, a); }

	void SetDensityScalar(IScalar* density){ _density = density; }
	void SetPressureScalar(IScalar* pressure){ _pressure = pressure; }

	void SetViscosityCoefficient(float mu){ _mu = mu; }
};

#endif