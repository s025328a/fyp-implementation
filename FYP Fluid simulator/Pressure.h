#ifndef PRESSURE_H
#define PRESSURE_H

#include "IScalar.h"
#include "Particle.h"
#include "ISmoothingKernel.h"
#include <math.h>

class Pressure// : public IScalar
{

public:
	float Calculate(Particle* particle);
};

#endif