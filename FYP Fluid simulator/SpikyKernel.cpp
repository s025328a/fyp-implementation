#include "SpikyKernel.h"

//SpikyKernel::SpikyKernel(float scale)
//{
//	//_h = h;
//	_scale = scale;
//}

SpikyKernel::SpikyKernel(float h)
{
	_h = h;
	SetValues();
}

void SpikyKernel::SetValues()
{
	_h2 = _h * _h;
	_value = _scale * (15 / (PI * pow(_h, 6)));
	_gradient = _scale * (-45 / (PI * pow(_h, 6)));
}

void SpikyKernel::SetKernelRadius(float h)
{
	_h = h;
	SetValues();
}

float SpikyKernel::Value(Vector3D rij, float h)
{
	if (rij.LengthSquared() <= _h2)
	{
		float r = rij.Length();
		//return _scale * (15 / (PI * pow(h, 6))) * pow(h - r, 3);
		return (_h - r) * (_h - r) * (_h - r) * _value;
	}
	return 0;
}

Vector3D SpikyKernel::Gradient(Vector3D rij, float h)
{
	if (rij.LengthSquared() <= _h2)
	{
		float r = rij.Length();
		//float dy = _scale * (-45 / (PI * pow(h, 6))) * (h - r) * (h - r);
		float dy = (_h - r) * (_h - r) * _gradient;

		if (dy < 0)
			dy *= -1.0f;

		return dy * rij.UnitVector();
	}
	return Vector3D(0, 0, 0);
}