#ifndef SIMULATOR_H
#define SIMULATOR_H

#include "Particle.h"
#include "IGraphics.h"

#include "ISmoothingKernel.h"
#include "Poly6Kernel.h"
#include "SpikyKernel.h"
#include "ViscosityKernel.h"
#include "SplineKernel.h"

#include "Fluid.h"
#include "IGrid.h"
#include "UniformGrid.h"
#include "GridCell.h"
#include "Vector3D.h"

#include "IForce.h"
#include "Gravity.h"
#include "PressureGradient.h"
#include "ViscosityMuller.h"
#include "SurfaceTension.h"

#include "IScalar.h"
#include "Density.h"
#include "Pressure.h"
#include "Colour.h"

#include <vector>
#include <memory>
#include <algorithm>
#include <Windows.h>
#include <fstream>
#include <iostream>
#include "psapi.h"

//#include "tbb\concurrent_vector.h"
#include "tbb/parallel_sort.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
#include "tbb/task_group.h"

using namespace std;
using namespace tbb;

struct FluidProperties
{
	Vector3D position;
	Vector3D volume;
	float k;
	float rho0;
	float mu;
	float mass_scale;
	Int3 size;
	//XMFLOAT4 colour;
};


inline bool CompareIndex(shared_ptr<Particle>& p1, shared_ptr<Particle>& p2)
{
	return p1->GetIndex() < p2->GetIndex();
}

inline bool CompareOriginal(shared_ptr<Particle>& p1, shared_ptr<Particle>& p2)
{
	return p1->_original && !p2->_original;
}

class Simulator
{
private:
	task_scheduler_init _task_scheduler;
	int _num_threads;
	int _num_particles;
	int _original_particles;

	bool _reset = false;

	float _h = 0.0f; // kernel support radius
	UniformGrid* _grid;
	IGraphics* _graphics;
	vector<shared_ptr<Particle>> _particles;
	vector<shared_ptr<Particle>> _boundary;

	vector<int> _avg_neighbours;

	ISmoothingKernel* _poly6;
	IForce* _pressure;

	vector<IForce*> _forces;
	vector<IScalar*> _scalars;
	vector<ISmoothingKernel*> _kernels;
	
	int _size;

	//float _gravity = 1.0f;
	float _grav_strength = 9.81f;
	Vector3D _gravity = Vector3D(0.0f, -_grav_strength, 0.0f);
	bool gravity_centre = false;

	float _slow_down = 1.0f;
	float _wall_damping = 1.0f;
	
	float _total_time;

	float _t;
	float _scale = 1.0f;
	float _timestep_mod;

	bool _parallel;

	float delays[7];
	int _frame_count = 0;

	Fluid* fluid;
	Vector3D volume;

	Vector3D _step;

	Vector3D _max_net_force = Vector3D(0, 0, 0);
	Vector3D _max_velocity = Vector3D(0, 0, 0);
	float _max_density = 0.0f;
	int _max_neighbours;
	float _nearest_neighbour = 10.0f;
	vector<float> _neighbour_distances;
	int _grid_construction;
	float _default_density;
	//float _max_position_change = 0.0f;

	int _kernel_particles;

	int _scenario = -1;

	vector<Fluid*> _fluids;

	float _sim_scale = 0.1f;
	bool _dropped = false;

	float _viscosity_values[3];
	int _viscosity_index = 0;

public:
	Simulator();
	~Simulator();

	void Initialize();

	void UpdateIndices();
	void SortParticles();
	void InsertParticles();

	void UpdateScalars();
	void UpdateForces();
	void UpdatePositions(float t);

	void UpdateScalar(int i, vector<shared_ptr<Particle>>& particles);
	void UpdateBoundaryScalar(int i);
	void UpdateScalar(int i);
	void UpdateForce(int i);

	void UpdateSerial(float t);
	void UpdateParallel(float t);

	void UpdateForcesAndPositions(float t);
	
	void InitBoundary(Vector3D step, float mass, float k);
	void CreateFluid(FluidProperties properties, Fluid* fluid, float spacing, float skew);
	void CreateGrid(Vector3D pos, Vector3D size, float h);

	void LoadScenario(int scenario);

	void ClearForces();

	void LoadCubeSplash();
	void LoadWaveBreak();
	void LoadSurfaceTension();

	void DropCube();
	void ChangeViscosity();

	//void CalculateScalars(shared_ptr<Particle>& p, shared_ptr<Particle>& neighbour);
	void CalculateForces(Vector3D& net, shared_ptr<Particle>& p, shared_ptr<Particle>& neighbour);
	void CalculateScalars(float& density, Vector3D& normal, Vector3D& position, shared_ptr<Particle>& neighbour);

	void CalculateScalars(shared_ptr<Particle>& particle);
	void BoundaryForce(Vector3D& net, shared_ptr<Particle>& particle, shared_ptr<Particle>& boundary_particle);

	void BoundaryCheck(shared_ptr<Particle>& particle, Vector3D grid_pos, Vector3D grid_size, float damping);

	bool KeyboardInput(MSG msg);

	void Update(float t);
	void Draw();
	void SetGraphics(IGraphics* graphics);
	void CleanUp();

	void Reset();
	void SaveTesting();
};

#endif