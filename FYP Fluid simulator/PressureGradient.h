#ifndef PRESSURE_GRADIENT_H
#define PRESSURE_GRADIENT_H

#include "IForce.h"
#include "Particle.h"

class PressureGradient : public IForce
{
private:
	ISmoothingKernel* _kernel;

public:
	PressureGradient();
	PressureGradient(ISmoothingKernel* kernel);
	
	string GetId(){ return "PressureGradient"; }
	Vector3D Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j);
};

#endif