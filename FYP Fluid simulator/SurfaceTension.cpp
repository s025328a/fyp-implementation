#include "SurfaceTension.h"

SurfaceTension::SurfaceTension(ISmoothingKernel* kernel)
{
	_kernel = kernel;
	//_curvature_kernel = curvature_kernel;
}


Vector3D SurfaceTension::Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j)
{
	float gamma = 3.0f;
	//Vector3D normal = i->GetColourGradient();
	float rho0 = i->GetFluid()->GetRestDensity();
	float h = 0.25f;

	//Vector3D cohesion = Vector3D(0, 0, 0);
	//Vector3D curvature = Vector3D(0, 0, 0);

	Vector3D rij = i->GetPosition() - j->GetPosition();

	Vector3D cohesion = i->GetMass() * j->GetMass() * _kernel->Value(rij, core) * rij.UnitVector();
	Vector3D curvature = h * i->GetMass() * (i->GetColourGradient() - j->GetColourGradient());
	//Vector3D curvature = Vector3D(0, 0, 0);

	float K = 2.0f * -gamma * rho0 / (i->GetDensity() + j->GetDensity());

	return K * /*(i->GetMass() / i->GetDensity())* */  (cohesion + curvature);

	/*vector<shared_ptr<Particle>> &neighbours = particle->GetNeighbours();

	for (int i = 0; i < neighbours.size(); ++i)
	{
		shared_ptr<Particle> j = neighbours[i];

		while (j != nullptr)
		{
			Vector3D rij = position - j->GetPosition();

			Vector3D cohesion = -gamma * j->GetMass() * _kernel->Value(rij, core) * rij.UnitVector();
			Vector3D curvature = -gamma * h * mass * (normal - j->GetColourGradient());

			float K = 2 * rho0 / (rhoi + j->GetDensity());

			total = total + K * (cohesion + curvature);
			j = j->Next();
		}
	}*/
		
	//return total;
}