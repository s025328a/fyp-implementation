#include <windows.h>
#include "resource.h"
#include "Simulator.h"
#include "IGraphics.h"
#include "ParticleGraphics.h"


HINSTANCE               g_hInst = NULL;
HWND                    g_hWnd = NULL;

Simulator* _simulator;
ParticleGraphics* _graphics;

float t;

HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
void CleanUp();
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
    UNREFERENCED_PARAMETER( hPrevInstance );
    UNREFERENCED_PARAMETER( lpCmdLine );

    if( FAILED( InitWindow( hInstance, nCmdShow ) ) )
        return 0;

	_graphics = new ParticleGraphics();

    if( FAILED( _graphics->InitGraphics(g_hWnd, hInstance) ) )
    {
        return 0;
    }

	_simulator = new Simulator();
	_simulator->Initialize();
	_simulator->SetGraphics(_graphics);

	t = 17.0f / 10000.0f;

	bool delay = true;

    MSG msg = {0};
    while( WM_QUIT != msg.message )
    {
		ULONGLONG start = GetTickCount64();
        if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
			bool handled = false;

			if (msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST)
			{
				handled = _simulator->KeyboardInput(msg);
			}
			else if (WM_QUIT == msg.message)
				break;

			if (!handled)
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
        }
        else
        {
            //_graphics->Render();
			_simulator->Update(t);
			//_simulator->Draw();

			/*if (delay)
			{
				Sleep(500);
				delay = false;
			}*/

			ULONGLONG end = GetTickCount64();
			/*if (end - start < 17)
			{
				Sleep(17 - (end - start));
				t = 17.0f / 10000.0f;
			}
			else
			{*/
				t = (float)(end - start) / 1000.0f;
			//}
        }
    }

	CleanUp();

    return (int)msg.wParam;
}


HRESULT InitWindow( HINSTANCE hInstance, int nCmdShow )
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof( WNDCLASSEX );
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon( hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
    wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
    wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
    wcex.lpszMenuName = NULL;
#if defined(_DEBUG)
    wcex.lpszClassName = L"SimulatorWindowClass";
#else
	wcex.lpszClassName = "SimulatorWindowClass";
#endif
    wcex.hIconSm = LoadIcon( wcex.hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
    if( !RegisterClassEx( &wcex ) )
        return E_FAIL;

    // Create window
    g_hInst = hInstance;
    RECT rc = { 0, 0, 1280, 720 };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );

#if defined(_DEBUG)
    g_hWnd = CreateWindow( L"SimulatorWindowClass", L"FYP Fluid Simulator",
                           WS_OVERLAPPEDWINDOW,
                           CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
                           NULL );
#else
	g_hWnd = CreateWindow( "SimulatorWindowClass", "FYP Fluid Simulator",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
		NULL );
#endif

    if( !g_hWnd )
        return E_FAIL;

    ShowWindow( g_hWnd, nCmdShow );

    return S_OK;
}


void CleanUp()
{
	_simulator->CleanUp();
}


LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch( message )
    {
        case WM_PAINT:
            hdc = BeginPaint( hWnd, &ps );
            EndPaint( hWnd, &ps );
            break;

        case WM_DESTROY:
            PostQuitMessage( 0 );
            break;

        default:
            return DefWindowProc( hWnd, message, wParam, lParam );
    }

    return 0;
}