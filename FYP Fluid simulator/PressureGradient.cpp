#include "PressureGradient.h"

PressureGradient::PressureGradient(ISmoothingKernel* kernel)
{
	_kernel = kernel;
}

Vector3D PressureGradient::Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j)
{	
	//vector <shared_ptr<Particle>>& neighbours = particle->GetNeighbours();

	Vector3D force = j->GetMass() * ((i->GetPressure() + j->GetPressure()) / (2.0f * j->GetDensity())) * _kernel->Gradient(i->GetPosition() - j->GetPosition(), core);
	//Vector3D force = (((i->GetPressure() / (i->GetDensity() * i->GetDensity())) + (j->GetPressure() / (j->GetDensity() * j->GetDensity()))) * _kernel->Gradient(i->GetPosition() - j->GetPosition(), core));
		
	Vector3D pressure = -(i->GetMass() / i->GetDensity()) * force;

	return pressure;
}