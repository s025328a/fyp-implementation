#ifndef IGRAPHICS_H
#define IGRAPHICS_H

#include "Particle.h"
#include <vector>

using namespace std;

class IGraphics
{
public:
	virtual void Prepare() = 0;
	virtual void DrawParticles(vector<shared_ptr<Particle>> particles, float h) = 0;
	virtual void Draw() = 0;
	virtual void DrawGrid(Vector3D position, Vector3D bounds) = 0;
	//virtual HRESULT InitGraphics(HWND& hWnd, HINSTANCE hInst) = 0;
	virtual void CleanUp() = 0;
};

#endif