#ifndef DENSITY_H
#define DENSITY_H

#include "IScalar.h"
#include "Particle.h"
#include "ISmoothingKernel.h"

class Density : public IScalar
{
private:
	ISmoothingKernel* _kernel;

public:
	Density();
	Density(ISmoothingKernel* kernel);
	
	float Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j);
};

#endif