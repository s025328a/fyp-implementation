#include "Simulator.h"

void Simulator::Draw()
{
	if (_graphics != NULL)
	{
		_graphics->Prepare();
		_graphics->DrawParticles(_particles, _h);
		//_graphics->DrawParticles(_boundary);
		//_graphics->DrawGrid(_grid->GetPosition(), _grid->GetSize());
		_graphics->Draw();
	}
}

void Simulator::SetGraphics(IGraphics* graphics)
{
	_graphics = graphics;
}

void Simulator::Initialize()
{
	//_num_threads = task_scheduler_init::default_num_threads();
	tbb:task_scheduler_init init(_num_threads = tbb::task_scheduler_init::default_num_threads());
	//task_scheduler_init init(2);

	_parallel = false;
	
	_kernel_particles = 20;
	

	LoadScenario(0);

	for (int i = 0; i < 7; ++i)
	{
		delays[i] = 0;
	}

	_total_time = GetTickCount64();
}

void Simulator::CreateFluid(FluidProperties properties, Fluid* fluid, float spacing, float skew)
{
	int num_particles = properties.size.x * properties.size.y * properties.size.z;
	
	if (fluid == nullptr)
	{
		fluid = new Fluid();
		float mass = (properties.rho0 * properties.mass_scale * properties.volume.x * properties.volume.y * properties.volume.z) / (float)num_particles;
		fluid->Initialize(mass, properties.rho0, properties.mu, properties.k);
		fluid->SetColour(0.0f, 0.75f, 0.0f, 1.0f);

		_fluids.push_back(fluid);
		this->fluid = fluid;
	}

	//properties.volume = 1.8f * properties.volume;
	//Vector3D step = 1.0f * Vector3D(properties.volume.x / (float)(properties.size.x - 1), properties.volume.y / (float)(properties.size.y - 1), properties.volume.z / (float)(properties.size.z - 1));

	Vector3D step = Vector3D(spacing * _h, spacing * _h, spacing * _h);

	/*float h = cbrt((3.0f * properties.volume.x * properties.volume.y * properties.volume.z * (float)_kernel_particles) / (4.0f * PI * _num_particles));
	if (h > _h)
	{
		_h = h;
	}*/

	_default_density = _poly6->Value(Vector3D(0, 0, 0), _h) * fluid->GetMass();

	int index = 0;
	float scale = skew;
	for (int i = 0; i < properties.size.y; ++i)
	{
		int index = properties.size.y - (i + 1);
		float zx_scale = pow(scale, index);
		for (int j = 0; j < properties.size.z; ++j)
		{
			for (int k = 0; k < properties.size.x; ++k)
			{
				shared_ptr<Particle> particle = std::make_shared<Particle>(Particle(Vector3D(properties.position.x + zx_scale * (k * step.x - (properties.volume.x / 2.0f)), properties.position.y + (i * step.y), properties.position.z + zx_scale * (j * step.z - (properties.volume.z / 2.0f))), fluid, index, _h));
				if (_num_particles > 0)
				{
					particle->_original = false;
				}
				particle->SetGrid((IGrid*&)_grid);
				particle->RecalculateIndex();
				particle->SetDefaultDensity(_default_density);
				_particles.push_back(particle);
				//index++;
			}
		}
	}
	_num_particles = _particles.size();
}

void Simulator::CreateGrid(Vector3D pos, Vector3D size, float h)
{
	ULONGLONG start = GetTickCount64();

	_grid = new UniformGrid(h, pos, size);
	_grid->Initialize();

	ULONGLONG end = GetTickCount64();
	_grid_construction = end - start;
}

void Simulator::InitBoundary(Vector3D step, float mass, float k)
{
	Vector3D bounds = _grid->GetSize();
	Vector3D pos = _grid->GetPosition();
	
	Vector3D grid_step = step;


	Vector3D bound_size = Vector3D(bounds.x / grid_step.x, bounds.y / grid_step.y, bounds.z / grid_step.z);

	grid_step.x = grid_step.x * bound_size.x / floor(bound_size.x);	
	grid_step.y = grid_step.y * bound_size.y / floor(bound_size.y);
	grid_step.z = grid_step.z * bound_size.z / floor(bound_size.z);

	Fluid* bounds_fluid = new Fluid();
	bounds_fluid->Initialize(mass, 1000.0f, 3.5f, k);

	int inclusion = 0;

	for (int i = 0; i <= bound_size.x; ++i)
	{
		for (int j = 0; j <= bound_size.y; ++j)
		{
			for (int k = 0; k <= bound_size.z; ++k)
			{
				if (i <= inclusion || (i + 1 + inclusion) * grid_step.x > bounds.x ||
					j <= inclusion || (j + 1 + inclusion) * grid_step.y > bounds.y ||
					k <= inclusion || (k + 1 + inclusion) * grid_step.z > bounds.z)
				{
					_boundary.push_back(std::make_shared<Particle>(Particle(pos + Vector3D(i * grid_step.x, j * grid_step.y, k * grid_step.z), bounds_fluid, 0, _h)));
				}
			}
		}
	}
	
	int boundary_size = _boundary.size();

	for (int i = 0; i < boundary_size; ++i)
	{
		_boundary[i]->SetGrid((IGrid*&)_grid);
		_boundary[i]->RecalculateIndex();
	}

	_grid->AddBoundaryStartIndex(_boundary[0]->GetIndex(), 0);

	if (_parallel)
	{
		tbb::parallel_sort(_boundary.begin(), _boundary.end(), CompareIndex);
		tbb::parallel_for(0, boundary_size - 1, 1, [=](int i)
		{
			if (_boundary[i]->GetIndex() != _boundary[i + 1]->GetIndex())
			{
				_grid->AddBoundaryEndIndex(_boundary[i]->GetIndex(), i);
				_grid->AddBoundaryStartIndex(_boundary[i + 1]->GetIndex(), i + 1);
			}
			_grid->AddBoundaryParticle(_boundary[i]);
		});
	}
	else
	{
		std::sort(_boundary.begin(), _boundary.end(), CompareIndex);
		for (int i = 0; i < boundary_size - 1; ++i)
		{
			if (_boundary[i]->GetIndex() != _boundary[i + 1]->GetIndex())
			{
				_grid->AddBoundaryEndIndex(_boundary[i]->GetIndex(), i);
				_grid->AddBoundaryStartIndex(_boundary[i + 1]->GetIndex(), i + 1);
			}
			_grid->AddBoundaryParticle(_boundary[i]);
		}
	}
	_grid->AddBoundaryEndIndex(_boundary[boundary_size - 1]->GetIndex(), boundary_size - 1);
	_grid->AddBoundaryParticle(_boundary[boundary_size - 1]);	

	_grid->CalculateBoundaryNeighbours();	

	for (int i = 0; i < _boundary.size(); ++i)
	{
		_boundary[i]->UpdateNeighbours();
		//vector<IndexRange>& neighbours = _boundary[i]->GetBoundaryNeighbourIndices();

		////_boundary[i]->UpdateNeighbours();

		//float density = 0.0f;

		//for (int j = 0; j < neighbours.size(); ++j)
		//{
		//	for (int k = neighbours[j].start; k <= neighbours[j].end; ++k)
		//	{
		//		shared_ptr<Particle>& neighbour = _boundary[k];
		//		float distance = (_boundary[i]->GetPosition() - neighbour->GetPosition()).LengthSquared();

		//		if (distance < (_h * _h))
		//		{
		//			density = density + _poly6->Value(_boundary[i]->GetPosition() - neighbour->GetPosition(), core);					
		//		}
		//	}
		//}
		//_boundary[i]->SetDefaultDensity(density * _boundary[i]->GetMass());
		_boundary[i]->SetDefaultDensity(328);
	}
}

///
/// Updates and draws all particles
///
void Simulator::Update(float t)
{
	//_t /= 10.0f;
	//t = 0.001f;


	//ULONGLONG start = GetTickCount64();
	
	if (_num_particles > 0)
	{
		if (_parallel)
		{
			UpdateParallel(t);
		}
		else
		{
			UpdateSerial(t);
		}
	}
	
	/*ULONGLONG end = GetTickCount64();
	int time = end - start;*/

	ULONGLONG start = GetTickCount64();
	// STEP 6 (Should likely be done in serial).
	Draw();
	ULONGLONG end = GetTickCount64();
	int time = end - start;

	delays[6] = time;

	_frame_count++;
}

void Simulator::UpdateSerial(float t)
{
	ULONGLONG start = GetTickCount64();

	UpdateIndices();
	ULONGLONG end = GetTickCount64();
	int time = end - start;
	delays[0] += time;

	start = GetTickCount64();
	SortParticles();
	end = GetTickCount64();
	time = end - start;
	delays[1] += time;

	start = GetTickCount64();
	InsertParticles();
	end = GetTickCount64();
	time = end - start;
	delays[2] += time;

	start = GetTickCount64();
	UpdateScalars();
	end = GetTickCount64();
	time = end - start;
	delays[3] += time;
	/*int size = _size / 3;

	float avg_density = 0.0f;

	vector<float> densities(size * size * size);

	int count = 0;

	for (int i = 0; i < size; ++i)
	{
	for (int j = 0; j < size; ++j)
	{
	for (int k = 0; k < size; ++k)
	{
	densities[count] = _particles[i + _size * j + size * size * k]->GetDensity();
	count++;
	}
	}
	}

	for (int i = 0; i < _num_particles; ++i)
	{
	avg_density += _particles[i]->GetDensity();
	}

	avg_density /= _num_particles;*/

	start = GetTickCount64();
	UpdateForces();
	end = GetTickCount64();
	time = end - start;
	delays[4] += time;

	start = GetTickCount64();
	UpdatePositions(_t);
	end = GetTickCount64();
	time = end - start;
	delays[5] += time;
}

void Simulator::UpdateParallel(float t)
{
	tbb::task_group tg;

	// STEP 1 Calculate Z-index for eavery particle, and then
	// Sort particles based on Z-index (could be done in serial?)

	ULONGLONG start = GetTickCount64();

	tg.run([&]() { UpdateIndices(); });
	tg.wait();

	ULONGLONG end = GetTickCount64();
	int time = end - start;
	delays[0] += time;

	start = GetTickCount64();

	tg.run([&]() { SortParticles(); });
	tg.wait();

	end = GetTickCount64();
	time = end - start;
	delays[1] += time;

	// SYNCHRONIZE and split particles up based on thread count.

	// STEP 2 Sort particles into grid, i.e. based on Z-index.
	// Potentially don't clear particles, but instead only move
	// particles which have changed grid cell.

	start = GetTickCount64();

	tg.run([&]() { InsertParticles(); });
	tg.wait();

	end = GetTickCount64();
	time = end - start;
	delays[2] += time;

	/*ULONGLONG end = GetTickCount64();
	int time = end - start;*/

	//start = GetTickCount64();

	// STEP 3 Update density, pressure, colour gradient.
	// Possibly change back to old method using IScalar?

	start = GetTickCount64();

	tg.run([&]() { UpdateScalars(); });
	tg.wait();

	end = GetTickCount64();
	time = end - start;
	delays[3] += time;

	// SYNCHRONIZE Force calculations require density/pressure values.

	// STEP 4 Calculate individual/net forces.

	start = GetTickCount64();

	tg.run([&]() { UpdateForces(); });
	tg.wait();

	end = GetTickCount64();
	time = end - start;
	delays[4] += time;

	// STEP 5 Update acceleration, velocity and position.
	// Check for collisions with boundary and adjust 
	// position/velocity accordingly.

	start = GetTickCount64();

	tg.run([&]() { UpdatePositions(_t); });
	tg.wait();

	end = GetTickCount64();
	time = end - start;
	delays[5] += time;
}

void Simulator::UpdateIndices()
{
	if (_parallel)
	{
		tbb::parallel_for(0, _num_particles, 1, [=](int i)
		{
			_particles[i]->RecalculateIndex();
		});
	}
	else
	{
		for (int i = 0; i < _num_particles; ++i)
		{
			_particles[i]->RecalculateIndex();
		}
	}
}

///
/// Sorts particles based on their index value
///
void Simulator::SortParticles()
{
	if (_parallel)
	{
		tbb::parallel_sort(_particles.begin(), _particles.end(), CompareIndex);
	}
	else
	{
		std::sort(_particles.begin(), _particles.end(), CompareIndex);
	}
}

///
/// Insert particles into grid
///
void Simulator::InsertParticles()
{
	_grid->ClearArrayIndices();
	_grid->AddStartIndex(_particles[0]->GetIndex(), 0);
	
	if (_parallel)
	{
		tbb::parallel_for(0, _num_particles - 1, 1, [=](int i)
		{
			if (_particles[i]->GetIndex() != _particles[i + 1]->GetIndex())
			{
				_grid->AddEndIndex(_particles[i]->GetIndex(), i);
				_grid->AddStartIndex(_particles[i + 1]->GetIndex(), i + 1);
			}
			_grid->AddParticle(_particles[i]);
		});
	}
	else
	{
		for (int i = 0; i < _num_particles - 1; ++i)
		{
			if (_particles[i]->GetIndex() != _particles[i + 1]->GetIndex())
			{
				_grid->AddEndIndex(_particles[i]->GetIndex(), i);
				_grid->AddStartIndex(_particles[i + 1]->GetIndex(), i + 1);
			}
			_grid->AddParticle(_particles[i]);
		}
	}

	_grid->AddEndIndex(_particles[_num_particles - 1]->GetIndex(), _num_particles - 1);
	_grid->AddParticle(_particles[_num_particles - 1]);
}


void Simulator::UpdateScalar(int i, vector<shared_ptr<Particle>>& particles)
{
	shared_ptr<Particle>& p = particles[i];

	Vector3D position = p->GetPosition();
	float mass = p->GetMass();

	p->UpdateNeighbours();
	vector<IndexRange> neighbours = p->GetNeighbourIndices();
	float density = 0.0f;
	Vector3D normal = Vector3D(0, 0, 0);

	int neighbour_count = 0;

	for (int j = 0; j < neighbours.size(); ++j)
	{
		for (int k = neighbours[j].start; k <= neighbours[j].end; ++k)
		{
			shared_ptr<Particle>& neighbour = particles[k];
			float distance = (position - neighbour->GetPosition()).LengthSquared();

			if (distance < (_h * _h))
			{
				density = density + _poly6->Value(position - neighbour->GetPosition(), core);
				normal = normal + (neighbour->GetMass() * _poly6->Gradient(position - neighbour->GetPosition(), core) / neighbour->GetDensity());
				neighbour_count++;
			}
		}
	}

	//_avg_neighbours.push_back(neighbour_count);

	//density = density + _poly6->Value(Vector3D(0, 0, 0), core);
	
	p->SetDensity(density);
	p->CalculatePressure();
	p->SetColourGradient(normal);
}

void Simulator::UpdateScalar(int i)
{
	shared_ptr<Particle>& p = _particles[i];

	Vector3D position = p->GetPosition();
	float mass = p->GetMass();

	p->UpdateNeighbours();
	vector<IndexRange> neighbours = p->GetNeighbourIndices();
	float density = 0.0f;
	Vector3D normal = Vector3D(0, 0, 0);

	int neighbour_count = 0;

	for (int j = 0; j < neighbours.size(); ++j)
	{
		for (int k = neighbours[j].start; k <= neighbours[j].end; ++k)
		{
			if (k != i)
			{
				shared_ptr<Particle>& neighbour = _particles[k];
				float distance = (position - neighbour->GetPosition()).LengthSquared();

				/*if (distance < _nearest_neighbour_end)
				{
					_nearest_neighbour_end = distance;
				}*/

				_neighbour_distances.push_back(distance);

				if (distance < (_h * _h))
				{
					density = density + _poly6->Value(position - neighbour->GetPosition(), core);
					normal = normal + (neighbour->GetMass() * _poly6->Gradient(position - neighbour->GetPosition(), core) / neighbour->GetDensity());
					neighbour_count++;

					if (distance < _nearest_neighbour)
					{
						_nearest_neighbour = distance;
					}
				}
			}
		}
	}

	//_avg_neighbours.push_back(neighbour_count);

	//density = density + _poly6->Value(Vector3D(0, 0, 0), core);
	
	density *= mass;

	if (density > _max_density)
	{
		_max_density = density;
	}

	p->SetDensity(density);
	p->CalculatePressure();
	p->SetColourGradient(normal);
}

void Simulator::UpdateBoundaryScalar(int i)
{
	shared_ptr<Particle>& p = _boundary[i];
	p->UpdateNeighbours();
	vector<IndexRange> neighbours = p->GetNeighbourIndices();

	if (neighbours.size() > 0)
	{

		Vector3D position = p->GetPosition();
		float mass = p->GetMass();


		float density = 0.0f;
		//Vector3D normal = Vector3D(0, 0, 0);

		int neighbour_count = 0;

		for (int j = 0; j < neighbours.size(); ++j)
		{
			for (int k = neighbours[j].start; k <= neighbours[j].end; ++k)
			{
				shared_ptr<Particle>& neighbour = _particles[k];
				float distance = (position - neighbour->GetPosition()).LengthSquared();

				if (distance < (_h * _h))
				{
					density = density + _poly6->Value(position - neighbour->GetPosition(), core);
					//normal = normal + (neighbour->GetMass() * _poly6->Gradient(position - neighbour->GetPosition(), core) / neighbour->GetDensity());
					neighbour_count++;
				}
			}
		}

		//_avg_neighbours.push_back(neighbour_count);

		//density = density + _poly6->Value(Vector3D(0, 0, 0), core);

		p->SetDensity(density * mass);
		p->CalculatePressure();
		//p->SetColourGradient(normal);
	}
}

///
/// Updates scalars, i.e. density, pressure, colour
///
void Simulator::UpdateScalars()
{
	//int size = (int)_particles.size();
	_avg_neighbours.clear();
	//_nearest_neighbour_end = 10.0f;
	_neighbour_distances.clear();

	if (_parallel)
	{
		tbb::parallel_for(0, _num_particles, 1, [=](int i)
		{
			//UpdateScalar(i, _particles);
			UpdateScalar(i);
		});
	}
	else
	{
		for (int i = 0; i < _num_particles; ++i)
		{
			//UpdateScalar(i, _particles);
			UpdateScalar(i);
		}
		for (int i = 0; i < _boundary.size(); ++i)
		{
			//UpdateScalar(i, _boundary);
			UpdateBoundaryScalar(i);
		}
	}

	/*float avg = 0;
	int max = 0;
	for (int i = 0; i < _avg_neighbours.size(); ++i)
	{
		avg += _avg_neighbours[i];
		if (_avg_neighbours[i] > max)
			max = _avg_neighbours[i];
	}
	avg /= (float)_avg_neighbours.size();*/
}


void Simulator::UpdateForce(int i)
{
	Vector3D net = Vector3D(0, 0, 0);

	shared_ptr<Particle>& particle = _particles[i];
	Vector3D position = particle->GetPosition();
	vector<IndexRange> neighbours = particle->GetNeighbourIndices();
	vector<IndexRange> boundary_neighbours = particle->GetBoundaryNeighbourIndices();

	int neighbour_count = 0;

	for (int j = 0; j < neighbours.size(); ++j)
	{
		for (int k = neighbours[j].start; k <= neighbours[j].end; ++k)
		{			
			if (k != i && (position - _particles[k]->GetPosition()).LengthSquared() < (_h * _h))
			{
				neighbour_count++;
				for (int l = 0; l < _forces.size(); l++)
				{
					net = net + _forces[l]->Calculate(particle, _particles[k]);
				}
			}
		}
	}

	particle->_neighbour_count = neighbour_count;

	if (_boundary.size() > 0)
	{
		for (int j = 0; j < boundary_neighbours.size(); ++j)
		{
			try
			{
				for (int k = boundary_neighbours[j].start; k <= boundary_neighbours[j].end; ++k)
				{
					if ((position - _boundary[k]->GetPosition()).LengthSquared() < (_h * _h))
					{
						net = net + _pressure->Calculate(particle, _boundary[k]);
					}
				}
			}
			catch (...)
			{
				throw;
			}
		}
	}
	
	net = net + (particle->GetMass() * _gravity);	

	if (net.LengthSquared() > _max_net_force.LengthSquared())
	{
		_max_net_force = net;
	}

	particle->SetNetForce(net);
}


///
/// Updates forces, e.g. pressure gradient, viscosity, surface tension
///
void Simulator::UpdateForces()
{
	if (_parallel)
	{
		tbb::parallel_for(0, _num_particles, 1, [=](int i)
		{
			UpdateForce(i);
		});
	}
	else
	{
		for (int i = 0; i < _num_particles; ++i)
		{
			UpdateForce(i);
		}
	}
}

void Simulator::UpdateForcesAndPositions(float t)
{
	Vector3D grid_pos = _grid->GetPosition();
	Vector3D grid_size = _grid->GetSize();

	if (_parallel)
	{
		tbb::parallel_for(0, _num_particles, 1, [=](int i)
		{
			UpdateForce(i);
			_particles[i]->UpdatePosition(t / _slow_down);
			if (_particles[i]->GetVelocity().LengthSquared() > _max_velocity.LengthSquared())
			{
				_max_velocity = _particles[i]->GetVelocity();
			}
			BoundaryCheck(_particles[i], grid_pos, grid_size, _wall_damping);
		});
	}
	else
	{
		for (int i = 0; i < _num_particles; ++i)
		{
			UpdateForce(i);
			_particles[i]->UpdatePosition(t / _slow_down);
			if (_particles[i]->GetVelocity().LengthSquared() > _max_velocity.LengthSquared())
			{
				_max_velocity = _particles[i]->GetVelocity();
			}
			BoundaryCheck(_particles[i], grid_pos, grid_size, _wall_damping);
		}
	}
}


///
/// Updates acceleration, velocity and position and integrates over the current timestep
/// Also checks for boundary collisions
///
void Simulator::UpdatePositions(float t)
{
	Vector3D grid_pos = _grid->GetPosition();
	Vector3D grid_size = _grid->GetSize();
	
	if (_parallel)
	{
		parallel_for(0, _num_particles, 1, [=](int i)
		{
			_particles[i]->UpdatePosition(t / _slow_down);
			if (_particles[i]->GetVelocity().LengthSquared() > _max_velocity.LengthSquared())
			{
				_max_velocity = _particles[i]->GetVelocity();
			}
			BoundaryCheck(_particles[i], grid_pos, grid_size, _wall_damping);
		});
	}
	else
	{
		for (int i = 0; i < _num_particles; ++i)
		{
			_particles[i]->UpdatePosition(t / _slow_down);
			if (_particles[i]->GetVelocity().LengthSquared() > _max_velocity.LengthSquared())
			{
				_max_velocity = _particles[i]->GetVelocity();
			}
			BoundaryCheck(_particles[i], grid_pos, grid_size, _wall_damping);
		}
	}
}


///
/// Calculates pressure gradient force between fluid particles and boundary particles
///
void Simulator::BoundaryForce(Vector3D& net, shared_ptr<Particle>& particle, shared_ptr<Particle>& boundary_particle)
{
	if (boundary_particle != nullptr)
	{
		net = net + _pressure->Calculate(particle, boundary_particle);
		BoundaryForce(net, particle, boundary_particle->Next());
	}
}


///
/// Checks for simple collisions with the boundary, reverses the velocity perpendicular to the plane
///
void Simulator::BoundaryCheck(shared_ptr<Particle>& particle, Vector3D grid_pos, Vector3D grid_size, float damping)
{
	Vector3D position = particle->GetPosition();
	Vector3D velocity = particle->GetVelocity();

	float error = 0.01f;

	if (position.y < grid_pos.y + error)
	{
		position.y = grid_pos.y + error;
		velocity.y *= -damping;
	}

	if (position.y > grid_pos.y + grid_size.y - error)
	{
		position.y = grid_pos.y + grid_size.y - error;
		velocity.y *= -damping;
	}

	if (position.x < grid_pos.x + error)
	{
		position.x = grid_pos.x + error;
		velocity.x *= -damping;
	}

	if (position.x > grid_pos.x + grid_size.x - error)
	{
		position.x = grid_pos.x + grid_size.x - error;
		velocity.x *= -damping;
	}

	if (position.z < grid_pos.z + error)
	{
		position.z = grid_pos.z + error;
		velocity.z *= -damping;
	}

	if (position.z > grid_pos.z + grid_size.z - error)
	{
		position.z = grid_pos.z + grid_size.z - error;
		velocity.z *= -damping;
	}

	particle->SetPosition(position);
	particle->SetVelocity(velocity);
}

void Simulator::SaveTesting()
{
	ofstream myfile;

	myfile.open("Testing.txt");

	float total = 0;

	_total_time = (GetTickCount64() - _total_time) / 1000.0f;
	float sim_time = _frame_count * _t;

	for (int i = 0; i < 7; ++i)
	{
		delays[i] = delays[i] / _frame_count;
		total += delays[i];
	}

	float avg_density = 0;
	float avg_pressure = 0;
	Vector3D avg_velocity = Vector3D(0, 0, 0);
	float avg_neighbours = 0;
	float avg_neighbour_count = 0;

	float max_dens = 0;

	float avg_neighbour_distance = 0.0f;
	float min_distance = 10.0f;

	for (int i = 0; i < _neighbour_distances.size(); ++i)
	{
		float dist = sqrt(_neighbour_distances[i]);
		if (dist < min_distance)
		{
			min_distance = dist;
		}
		avg_neighbour_distance += dist;
	}

	_num_particles = _particles.size();
	avg_neighbour_distance /= _neighbour_distances.size();

	for (int i = 0; i < _num_particles; ++i)
	{
		shared_ptr<Particle>& p = _particles[i];

		avg_density += p->GetDensity();

		if (p->GetDensity() > max_dens)
		{
			max_dens = p->GetDensity();
		}

		avg_pressure += p->GetPressure();
		avg_velocity = avg_velocity + p->GetVelocity();
		avg_neighbour_count += p->_neighbour_count;

		if (p->_neighbour_count > _max_neighbours)
		{
			_max_neighbours = p->_neighbour_count;
		}
	}

	avg_density /= _num_particles;
	avg_pressure /= _num_particles;
	avg_velocity = avg_velocity / _num_particles;
	avg_neighbour_count /= _num_particles;

	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	DWORDLONG totalVirtualMem = memInfo.ullTotalPageFile;
	DWORDLONG virtualMemUsed = memInfo.ullTotalPageFile - memInfo.ullAvailPageFile;

	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;

	myfile << "Simulation parameters:";
	myfile << "\nNumber of particles: " << std::to_string(_num_particles);
	//myfile << "\nKernel particles: " << std::to_string(_kernel_particles);
	myfile << "\nKernel support radius: " << std::to_string(_h);
	myfile << "\nTimestep: " << std::to_string(_t);
	myfile << "\nTimestep Modifier: " << std::to_string(_timestep_mod);

	myfile << "\n\nFluid properties: ";
	myfile << "\nRest Density: " << std::to_string(fluid->GetRestDensity());
	myfile << "\nPressure stiffness: " << std::to_string(fluid->GetPressureStiffness());
	myfile << "\nViscosity coefficient: " << std::to_string(fluid->GetViscosityCoefficient());
	myfile << "\nMass: " << std::to_string(fluid->GetMass());
	myfile << "\nFluid starting volume: x: " << std::to_string(volume.x) << " y: " << std::to_string(volume.y) << " z: " << std::to_string(volume.z);

	myfile << "\n\nDelays (ms):\n";
	myfile << "Grid construction time: " << std::to_string(_grid_construction);
	myfile << "\nUpdate indices: " << std::to_string(delays[0]) << "\n";
	myfile << "Sort Particles: " << std::to_string(delays[1]) << "\n";
	myfile << "Insert into grid: " << std::to_string(delays[2]) << "\n";
	myfile << "Update Scalars: " << std::to_string(delays[3]) << "\n";
	myfile << "Update Forces: " << std::to_string(delays[4]) << "\n";
	myfile << "Update Positions: " << std::to_string(delays[5]) << "\n";
	myfile << "Draw: " << std::to_string(delays[6]) << "\n";
	myfile << "Total: " << std::to_string(total) << "\n\n";

	myfile << "Total memory: " << std::to_string(totalVirtualMem / 1024);
	myfile << "\nTotal memory consumption: " << std::to_string(virtualMemUsed / 1024);
	myfile << "\nCurrent memory consumption: " << std::to_string(virtualMemUsedByMe / 1024);
	myfile << "\nGrid cells: " << std::to_string(_grid->GetCellCount());

	myfile << "\n\nSimulation time: " << std::to_string(sim_time);
	myfile << "\nActual time: " << std::to_string(_total_time);
	myfile << "\nDifference factor: " << std::to_string(_total_time / sim_time);

	myfile << "\n\nAverages: \n";
	myfile << "Default Density: " << std::to_string(_default_density);
	myfile << "\nDensity: " << std::to_string(avg_density);
	myfile << "\nPressure: " << std::to_string(avg_pressure);
	myfile << "\nNeighbours: " << std::to_string(avg_neighbour_count);
	myfile << "\nNeighbour Distance: " << std::to_string(avg_neighbour_distance);
	myfile << "\n% of h: " << std::to_string(100.0f * avg_neighbour_distance / _h);
	//myfile << "\nVelocity: " << std::to_string(avg_velocity.x) << " y: " << std::to_string(avg_velocity.y) << " z: " << std::to_string(avg_velocity.z);

	myfile << "\n\nMaximum:";
	myfile << "\nDensity: " << std::to_string(_max_density);
	myfile << "\nDensity (at end): " << std::to_string(max_dens);
	//myfile << "\nForce: " << std::to_string(_max_net_force.x) << " y: " << std::to_string(_max_net_force.y) << " z: " << std::to_string(_max_net_force.z);
	//myfile << "\nVelocity: " << std::to_string(_max_velocity.x) << " y: " << std::to_string(_max_velocity.y) << " z: " << std::to_string(_max_velocity.z);
	myfile << "\nPosition change per timestep: " << std::to_string(_max_velocity.Length() * _t);
	myfile << "\nIdeal position change: " << std::to_string(_h / 2.0f);
	myfile << "\nNeighbours: " << std::to_string(_max_neighbours);
	myfile << "\nClosest neighbour: " << std::to_string(sqrt(_nearest_neighbour));
	myfile << "\nClosest neighbour (% of h): " << std::to_string(100.0f * sqrt(_nearest_neighbour) / _h);
	myfile << "\n\nClosest neighbour (at end): " << std::to_string(min_distance);
	myfile << "\n(% of h): " << std::to_string(100.0f * min_distance / _h);

	myfile.close();
}


void Simulator::CleanUp()
{
	SaveTesting();

	if (_graphics) _graphics->CleanUp();

	for (int i = 0; i < _particles.size(); ++i)
	{
		_particles[i]->CleanUp();
		_particles[i].reset();
	}

	_particles.clear();
	_num_particles = 0;

	_grid->Reset();
	_grid = nullptr;
}

Simulator::Simulator()
{
	
}

void Simulator::Reset()
{
	if (_num_particles > _original_particles)
	{
		std::sort(_particles.begin(), _particles.end(), CompareOriginal);
		_particles.erase(_particles.begin() + _original_particles, _particles.end());
		_num_particles = _original_particles;
		_dropped = false;
	}

	for (int i = 0; i < _particles.size(); ++i)
	{
		_particles[i]->Reset();
	}

	if (_scenario == 2)
	{
		_grav_strength = 0.0f;
	}

	_gravity = Vector3D(0.0f, -_grav_strength, 0.0f);
}

void Simulator::LoadScenario(int scenario)
{
	if (scenario == _scenario)
	{
		Reset();
		return;
	}

	if (_particles.size() > 0)
	{
		for (int i = 0; i < _particles.size(); ++i)
		{
			_particles[i]->CleanUp();
			_particles[i].reset();
		}

		_particles.clear();
		_num_particles = 0;
	}

	if (_grid != nullptr)
	{
		_grid->Reset();
		_grid = nullptr;
	}

	switch (scenario)
	{
	case 0:
		LoadCubeSplash();
		break;
	case 1:
		LoadWaveBreak();
		break;
	case 2:
		LoadSurfaceTension();
		break;
	}

	_original_particles = _num_particles;
	_scenario = scenario;
}

bool Simulator::KeyboardInput(MSG msg)
{
	//if (msg.wParam == VK_SPACE)
	//{
	//	//_gravity *= -1.0f;
	//	gravity_centre = true;
	//	return true;
	//}
	


 	switch (msg.wParam)
	{
	case VK_UP:
		_gravity = Vector3D(0.0f, _grav_strength, 0.0f);
		break;
	case VK_DOWN:
		_gravity = Vector3D(0.0f, -_grav_strength, 0.0f);
		break;
	case VK_LEFT:
		_gravity = Vector3D(-_grav_strength, 0.0f, 0.0f);
		break;
	case VK_RIGHT:
		_gravity = Vector3D(_grav_strength, 0.0f, 0.0f);
		break;
	case VK_SPACE:
		Reset();
		break;
	case 0x44:
		DropCube();
		break;
	case 0x47:
		_grav_strength = 9.81f;
		_gravity = Vector3D(0.0f, -_grav_strength, 0.0f);
		break;
	case 0x56:
		if (msg.message == WM_KEYDOWN)
			ChangeViscosity();
		break;
	case 0x30:
		LoadScenario(0);
		break;
	case 0x31:
		LoadScenario(1);
		break;
	case 0x32:
		LoadScenario(2);
		break;
	}

	return false;
}

void Simulator::ClearForces()
{
	for (int i = 0; i < _forces.size(); ++i)
	{
		_forces[i] = nullptr;
	}

	for (int i = 0; i < _kernels.size(); ++i)
	{
		_kernels[i] = nullptr;
	}
	_forces.clear();
	_kernels.clear();
}

void Simulator::DropCube()
{
	if (!_dropped && _scenario == 0)
	{
		FluidProperties properties;
		//properties.k = 12.9f;
		//properties.mass_scale = 1.0f;
		//properties.mu = 1.0f;
		properties.position = _sim_scale * Vector3D(0, 2.5f, 0);
		properties.volume = _sim_scale * Vector3D(0.8f, 0.8f, 0.8f);
		//properties.rho0 = 1000.0f;
		properties.size.x = 8;
		properties.size.y = 8;
		properties.size.z = 8;

		CreateFluid(properties, _fluids[0], 1.0f, 1.01f);

		_dropped = true;
	}
}

void Simulator::ChangeViscosity()
{
	if (_scenario == 0)
	{
		_viscosity_index++;
		if (_viscosity_index > 2)
		{
			_viscosity_index = 0;
		}
		fluid->SetViscosityCoefficient(_viscosity_values[_viscosity_index]);
	}
}

void Simulator::LoadCubeSplash()
{
	_dropped = false;

	_grav_strength = 9.81f;
	_gravity = Vector3D(0.0f, -_grav_strength, 0.0f);

	ClearForces();
	
	_viscosity_values[0] = 1.0f;
	_viscosity_values[1] = 4.0f;
	_viscosity_values[2] = 12.5f;
	_viscosity_index = 1;

	FluidProperties properties;
	properties.k = 25.0f;
	properties.mass_scale = 1.0f;
	properties.mu = _viscosity_values[_viscosity_index];
	properties.position = _sim_scale * Vector3D(0, -0.9f, 0);
	properties.volume = _sim_scale * Vector3D(1.0f, 2.5f, 1.0f);
	properties.rho0 = 1000.0f;
	properties.size.x = 10;
	properties.size.y = 25;
	properties.size.z = 10;

	volume = properties.volume;
	
	_wall_damping = 0.67f;

	_h = cbrt((3.0f * properties.volume.x * properties.volume.y * properties.volume.z * (float)_kernel_particles) / (4.0f * PI * properties.size.x * properties.size.y * properties.size.z));
	_timestep_mod = 0.4f;
	_t = _timestep_mod * _h / sqrt(properties.k);
	
	ISmoothingKernel* spiky = new SpikyKernel(_h * 0.95f);
	_pressure = new PressureGradient(spiky);
	_forces.push_back(_pressure);
	_kernels.push_back(spiky);

	ISmoothingKernel* viscosityKernel = new ViscosityKernel(_h);
	IForce* viscosity = new ViscosityMuller(viscosityKernel);
	_forces.push_back(viscosity);
	_kernels.push_back(viscosityKernel);

	_poly6 = nullptr;
	_poly6 = new Poly6Kernel(_h * 0.95f, 1.0f);
	_kernels.push_back(_poly6);

	// CREATE GRID

	float tempX = 2.5f;
	float tempZ = 2.5f;

	Vector3D bounds = Vector3D(tempX * 2.0f, 5.0f, tempZ * 2.0f);
	Vector3D pos = Vector3D(-tempZ, -1.0f, -tempZ);

	bounds = _sim_scale  * bounds;
	pos = _sim_scale  * pos;

	/*Vector3D bounds = Vector3D(0.05f, 0.05f, 0.05f);
	Vector3D pos = Vector3D(-0.025f, -0.02f, -0.025f);*/

	CreateGrid(pos, bounds, _h);

	// CREATE FLUID

	CreateFluid(properties, nullptr, 1.0f, 1.015f);
}

void Simulator::LoadWaveBreak()
{
	_grav_strength = 9.81f;
	_gravity = Vector3D(0.0f, -_grav_strength, 0.0f);

	ClearForces();
	
	FluidProperties properties;
	properties.k = 12.0f;
	properties.mass_scale = 1.0f;
	properties.mu = 2.5f;
	properties.position = _sim_scale * Vector3D(0, -0.9f, 0);
	properties.volume = _sim_scale * Vector3D(0.5f, 2.8f, 1.2f);
	properties.rho0 = 998.29f;
	properties.size.x = 5;
	properties.size.y = 28;
	properties.size.z = 12;
	
	volume = properties.volume;

	_wall_damping = 1.0f;

	_h = cbrt((3.0f * properties.volume.x * properties.volume.y * properties.volume.z * (float)_kernel_particles) / (4.0f * PI * properties.size.x * properties.size.y * properties.size.z));
	_timestep_mod = 0.4f;
	_t = _timestep_mod * _h / sqrt(properties.k);
	
	ISmoothingKernel* spiky = new SpikyKernel(_h);
	_pressure = new PressureGradient(spiky);
	_forces.push_back(_pressure);
	_kernels.push_back(spiky);

	ISmoothingKernel* viscosityKernel = new ViscosityKernel(_h);
	IForce* viscosity = new ViscosityMuller(viscosityKernel);
	_forces.push_back(viscosity);
	_kernels.push_back(viscosityKernel);

	_poly6 = nullptr;
	_poly6 = new Poly6Kernel(_h, 1.0f);
	_kernels.push_back(_poly6);

	// CREATE GRID

	float tempX = 2.75f;
	float tempZ = 1.2f;

	Vector3D bounds = Vector3D(tempX * 2.0f, 5.0f, tempZ * 2.0f);
	Vector3D pos = Vector3D(-tempZ +0.6f, -1.0f, -tempZ + 0.3f);

	bounds = _sim_scale  * bounds;
	pos = _sim_scale  * pos;

	/*Vector3D bounds = Vector3D(0.05f, 0.05f, 0.05f);
	Vector3D pos = Vector3D(-0.025f, -0.02f, -0.025f);*/

	CreateGrid(pos, bounds, _h);

	// CREATE FLUID

	CreateFluid(properties, nullptr, 1.0f, 1.005f);
}

void Simulator::LoadSurfaceTension()
{
	_grav_strength = 0.0f;
	_gravity = Vector3D(0.0f, 0.0f, 0.0f);

	FluidProperties properties;
	properties.k = 23.8f;
	properties.mass_scale = 1.0f;
	properties.mu = 7.0f;
	properties.position = _sim_scale * Vector3D(0, 0.0f, 0);
	properties.volume = _sim_scale * Vector3D(1.0f, 1.0f, 1.0f);
	properties.rho0 = 1000.0f;
	properties.size.x = 12;
	properties.size.y = 12;
	properties.size.z = 12;

	volume = properties.volume;

	_wall_damping = 0.67f;

	float scale = 0.9f;

	_h = cbrt((3.0f * properties.volume.x * properties.volume.y * properties.volume.z * (float)_kernel_particles) / (4.0f * PI * properties.size.x * properties.size.y * properties.size.z));
	_timestep_mod = 0.4f;
	_t = _timestep_mod * _h / sqrt(properties.k);
	
	ClearForces();
	
	ISmoothingKernel* spiky = new SpikyKernel(scale * _h);
	_pressure = new PressureGradient(spiky);
	_forces.push_back(_pressure);
	_kernels.push_back(spiky);

	ISmoothingKernel* viscosityKernel = new ViscosityKernel(scale * _h);
	IForce* viscosity = new ViscosityMuller(viscosityKernel);
	_forces.push_back(viscosity);
	_kernels.push_back(viscosityKernel);

	ISmoothingKernel* splineKernel = new SplineKernel(_h);
	IForce* surfaceTension = new SurfaceTension(splineKernel);
	_forces.push_back(surfaceTension);
	_kernels.push_back(splineKernel);

	_poly6 = nullptr;
	_poly6 = new Poly6Kernel(_h * 0.9f, 1.0f);
	_kernels.push_back(_poly6);
	
	// CREATE GRID

	float tempX = 5.0f;
	float tempZ = 5.0f;

	Vector3D bounds = Vector3D(tempX * 2.0f, 4.0f, tempZ * 2.0f);
	Vector3D pos = Vector3D(-tempZ, -1.0f, -tempZ);

	bounds = _sim_scale  * bounds;
	pos = _sim_scale  * pos;

	/*Vector3D bounds = Vector3D(0.05f, 0.05f, 0.05f);
	Vector3D pos = Vector3D(-0.025f, -0.02f, -0.025f);*/

	CreateGrid(pos, bounds, _h);

	// CREATE FLUID

	CreateFluid(properties, nullptr, 0.87f, 1.00f);
}