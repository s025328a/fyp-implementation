#ifndef IFORCE_H
#define IFORCE_H

#include "Vector3D.h"
//#include "Particle.h"
#include "ISmoothingKernel.h"
#include <vector>
#include <memory>

using namespace std;

class Particle;

class IForce
{
public:
	virtual string GetId() = 0;
	//virtual void SetSmoothingKernel(ISmoothingKernel* kernel) = 0;
	virtual Vector3D Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j) = 0;
};

#endif