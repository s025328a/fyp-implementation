#ifndef DIRECT_INPUT_H
#define DIRECT_INPUT_H

#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")

#include <dinput.h>

class directInput
{
private:
	IDirectInputDevice8 *mouseInput;
	IDirectInputDevice8 *keyboardInput;
	LPDIRECTINPUT8 inputDevice;
	DIMOUSESTATE lastMouseState;

	bool leftDown;
	bool rightDown;

	float xChange;
	float yChange;

public:
	directInput();
	~directInput();

	void cleanUpInput();

	HRESULT initDirectInput(HINSTANCE hInst, HWND hWnd);

	bool getRightClick();
	bool getLeftClick();

	float getXChange();
	float getYChange();

	void updateMouseInput();
	void updateKeyboardInput();

	int checkNumKey();
	bool checkSpace();
};

#endif