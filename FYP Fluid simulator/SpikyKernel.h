#ifndef SPIKY_KERNEL_H
#define SPIKY_KERNEL_H

#include "ISmoothingKernel.h"
#include "Vector3D.h"
#include <math.h>

class SpikyKernel : public ISmoothingKernel
{
private:
	float _value;
	float _gradient;
	float _h2;

public:
	SpikyKernel(float h);
	//~SpikyKernel();

	//SpikyKernel(float scale);

	float Value(Vector3D rij, float h);
	Vector3D Gradient(Vector3D rij, float h);

	void SetValues();
	void SetKernelRadius(float h);
};

#endif