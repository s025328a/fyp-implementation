#ifndef ISCALAR_H
#define ISCALAR_H

#include <vector>
#include <memory>

using namespace std;

class Particle;

class IScalar
{
public:
	virtual float Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j) = 0;
};

#endif