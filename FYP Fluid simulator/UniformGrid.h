#ifndef UNIFORM_GRID_H
#define UNIFORM_GRID_H

#include "GridCell.h"
#include "Particle.h"
#include "Vector3D.h"
#include "IGrid.h"

#include <vector>
#include <memory>
#include <map>

using namespace std;

class GridCell;
//class Particle;

class UniformGrid : public IGrid
{
private:
	std::map<uint64_t, shared_ptr<GridCell>> _cell_dictionary;

	int _width;
	int _height;
	int _depth;

	Vector3D _size;
	Vector3D _position;
	float _cell_size;

	vector<shared_ptr<GridCell>> _cells;
	
	int _boundary_count = 0;


public:
	UniformGrid();
	UniformGrid(float cell_size, Vector3D position, Vector3D grid_size);

	void Initialize();

	void ClearParticles();
	void ClearIndices();

	void AddParticle(shared_ptr<Particle>& p);
	void AddBoundaryParticle(shared_ptr<Particle>& b);

	void ClearArrayIndices();
	
	Vector3D GetSize(){ return _size; }
	Vector3D GetPosition(){ return _position; }
	float GetCellSize(){ return _cell_size; }

	vector<shared_ptr<GridCell>> GetNeighbouringCells(int i, int j, int k);

	Int3 GetGridPosition(Vector3D position);

	void AddStartIndex(uint64_t index, int array_index);
	void AddEndIndex(uint64_t index, int array_index);
	void AddBoundaryStartIndex(uint64_t index, int array_index);
	void AddBoundaryEndIndex(uint64_t index, int array_index);

	void CalculateBoundaryNeighbours();

	void Reset();

	int GetCellCount()
	{
		return _cells.size();
	}
};

#endif