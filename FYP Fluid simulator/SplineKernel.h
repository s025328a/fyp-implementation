#ifndef SPLINE_KERNEL_H
#define SPLINE_KERNEL_H

#include "ISmoothingKernel.h"

class SplineKernel : public ISmoothingKernel
{
private:
	float _h2;
	float _value;
	float _temp;

public:
	SplineKernel(float h);

	float Value(Vector3D rij, float h);
	Vector3D Gradient(Vector3D rij, float h);

	void SetValues();
	void SetKernelRadius(float h);
};

#endif