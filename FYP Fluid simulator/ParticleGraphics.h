#ifndef PARTICLE_GRAPHICS_H
#define PARTICLE_GRAPHICS_H

#include "Particle.h"
#include "IGraphics.h"

#include <windows.h>
#include <d3d11.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include <vector>
#include <algorithm> 
#include "resource.h"

#include "DirectInput.h"

using namespace DirectX;

struct Vertex
{
	XMFLOAT3 position;
	//XMFLOAT4 colour;
	XMFLOAT3 normal;
};

struct ConstantBuffer
{
	XMMATRIX world;
	XMMATRIX view;
	XMMATRIX projection;
	XMFLOAT4 lightDir;
	XMFLOAT4 colour;
};

struct Camera
{
	XMFLOAT4 Eye;
	XMFLOAT4 At;
	XMFLOAT4 Up;
	XMFLOAT4 OriginalPos;
	float rotation[3];
};

class ParticleGraphics : public IGraphics
{
private:
	D3D_DRIVER_TYPE				_driverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL			_featureLevel = D3D_FEATURE_LEVEL_11_0;

	ID3D11Device*				_device = NULL;
	ID3D11DeviceContext*		_deviceContext = NULL;
	IDXGISwapChain*				_swapChain = NULL;
	ID3D11RenderTargetView*		_renderTargetView = NULL;
	ID3D11VertexShader*			_vertexShader = NULL;
	ID3D11PixelShader*			_pixelShader = NULL;
	ID3D11InputLayout*			_inputLayout = NULL;
	ID3D11RasterizerState*		_wireframeRasterizer = NULL;
	ID3D11RasterizerState*		_rasterizerState = NULL;
	ID3D11Buffer*				_constantBuffer = NULL;
	ID3D11Buffer*				_vertexBuffer = NULL;
	ID3D11Buffer*				_indexBuffer = NULL;
	ID3D11Buffer*				_cubeVertexBuffer = NULL;
	ID3D11Buffer*				_cubeIndexBuffer = NULL;
	ID3D11DepthStencilView*		_depthStencilView = NULL;
	ID3D11Texture2D*			_depthStencilBuffer = NULL;


	XMFLOAT4X4					_world;
	XMFLOAT4X4					_view;
	XMFLOAT4X4					_projection;

	XMFLOAT4					_lightDir;

	Camera						_cam;
	directInput*				_input;
		
	int index_count;
	int cube_index_count;

	HRESULT InitVertexData();
	HRESULT InitCubeData();

	XMFLOAT3 SphereValue(float angleX, float angleY, float size)
	{
		float y = cos(angleX) * size;
		float x = sin(angleY) * sin(angleX) * size;
		float z = cos(angleY) * sin(angleX) * size;

		return XMFLOAT3(x, y, z);
	}

public:
	//ParticleGraphics();
	//~ParticleGraphics();

	HRESULT InitGraphics(HWND& hWnd, HINSTANCE hInst);
	void DrawParticles(vector<shared_ptr<Particle>> particles, float h/*, Vector3D grid_pos, Vector3D grid_size*/);

	void Prepare();
	void Draw();

	//void DrawGrid(Vector3D position, Vector3D bounds);

	void DrawParticle(shared_ptr<Particle> particle);
	void DrawGrid(Vector3D position, Vector3D bounds);

	void CleanUp();

	void ErrorMessage()
	{
#if defined(_DEBUG) || defined(DEBUG)
		MessageBox(NULL, L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
#else
		MessageBox(NULL, "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
#endif

	}

	HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
	{
		HRESULT hr = S_OK;

		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
		#if defined( DEBUG ) || defined( _DEBUG )
		// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
		// Setting this flag improves the shader debugging experience, but still allows 
		// the shaders to be optimized and to run exactly the way they will run in 
		// the release configuration of this program.
		dwShaderFlags |= D3DCOMPILE_DEBUG;
		#endif

		ID3DBlob* pErrorBlob;
		hr = D3DCompileFromFile(szFileName, NULL, NULL, szEntryPoint, szShaderModel,
			dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
		if (FAILED(hr))
		{
			if (pErrorBlob != NULL)
				OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
			if (pErrorBlob) pErrorBlob->Release();
			return hr;
		}
		if (pErrorBlob) pErrorBlob->Release();

		return S_OK;
	}
};

#endif