#include "Pressure.h"

float Pressure::Calculate(Particle* particle)
{
	Fluid*& fluid = particle->GetFluid();
	float k = fluid->GetPressureStiffness();
	float rho0 = fluid->GetRestDensity();
	float rho = particle->GetDensity();
	
	float gamma = 1.0f;

	//float b = rho0 * 88.5 * 88.5 / gamma;

	float p = k * (rho0 / gamma) * (pow(rho / rho0, gamma) - 1.0f);
	
	particle->SetPressure(p);
	return p;
}