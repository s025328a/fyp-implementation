#ifndef GRIDCELL_H
#define GRIDCELL_H

#include "Vector3D.h"
//#include "Particle.h"
#include "UniformGrid.h"
#include <vector>
#include <memory>

using namespace std;

class UniformGrid;
class Particle;

class GridCell
{
private:
	IndexRange _range;
	IndexRange _boundary_range;
	vector<IndexRange> _boundary_neighbours;

	Vector3D _position;
	vector<shared_ptr<Particle>> _particles;

	vector<shared_ptr<GridCell>> _neighbour_cells;
	shared_ptr<Particle> _root_particle = nullptr;
	shared_ptr<Particle> _end_particle = nullptr;

	uint64_t _pIndex_root;
	uint64_t _pIndex_end;
	
	shared_ptr<Particle> _root_boundary = nullptr;
	shared_ptr<Particle> _end_boundary = nullptr;
	
	int _i;
	int _j;
	int _k;

	UniformGrid* _parent;
	
	uint64_t _index;

public:
	GridCell();
	~GridCell()
	{
		CleanUp();
	}
	GridCell(UniformGrid* parent, Vector3D position, int i, int j, int k);

	void AddParticle(shared_ptr<Particle>& p);
	void AddBoundaryParticle(shared_ptr<Particle>& b);
	void Clear();

	Int3 GetCellPosition()
	{
		return Int3(_i, _j, _k);
	}
	
	Vector3D GetPosition(){ return _position; }

	IndexRange GetIndexRange(){ return _range; }

	void SetRootIndex(uint64_t root){ _pIndex_root = root; }

	void SetNeighbourCells(vector<shared_ptr<GridCell>> neighbour_cells){ _neighbour_cells = neighbour_cells; }

	vector<shared_ptr<Particle>>& GetParticles(){ return _particles; }
	vector<shared_ptr<Particle>> GetNeighbourParticles();
	vector<shared_ptr<Particle>> GetBoundaryParticles();

	vector<uint64_t> GetParticleIndices();
	//vector<uint64_t> GetNeighbourIndices();
	vector<IndexRange> GetNeighbourIndices();
	vector<IndexRange>& GetBoundaryNeighbourIndices(){ return _boundary_neighbours; }
	void CalculateNeighbourBoundaryIndices();

	IndexRange GetBoundaryIndexRange(){ return _boundary_range; }

	vector<vector<shared_ptr<Particle>>> GetNeighbourParticleList();
	vector<shared_ptr<GridCell>> GetNeighbourCells(){ return _neighbour_cells; }

	shared_ptr<Particle>& GetRootParticle(){ return _root_particle; }
	shared_ptr<Particle>& GetRootBoundary(){ return _root_boundary; }

	void CalculateZIndex();
	uint64_t GetIndex(){ return _index; }

	void SetStartIndex(int index){ _range.start = index; }
	void SetEndIndex(int index){ _range.end = index; }

	void SetBoundaryStartIndex(int index){ _boundary_range.start = index; }
	void SetBoundaryEndIndex(int index){ _boundary_range.end = index; }

	void SetIndexRange(int start, int end)
	{ 
		_range.start = start;
		_range.end = end;
	}

	void CleanUp();
};

#endif