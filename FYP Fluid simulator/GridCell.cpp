#include "GridCell.h"

GridCell::GridCell(UniformGrid* parent, Vector3D position, int i, int j, int k)
{
	_parent = parent;
	_position = position;

	_i = i;
	_j = j;
	_k = k;

	CalculateZIndex();
}

void GridCell::AddParticle(shared_ptr<Particle>& p)
{
	p->SetNext(nullptr);
	//p->SetIndex(_index);
	if (_root_particle == nullptr)
	{
		_root_particle = p;
		_end_particle = p;
	}
	else
	{
		_end_particle->SetNext(p);
		_end_particle = p;
	}

	//_particles.push_back(p);
}

void GridCell::AddBoundaryParticle(shared_ptr<Particle>& b)
{
	b->SetNext(nullptr);
	//b->SetIndex(_index);
	if (_root_boundary == nullptr)
	{
		_root_boundary = b;
		_end_boundary = b;
	}
	else
	{
		_end_boundary->SetNext(b);
		_end_boundary = b;
	}
}

void GridCell::Clear()
{
	/*if (_particles.size() > 0)
		_particles.clear();*/

	if (_root_particle != nullptr)
		_root_particle = nullptr;

	if (_end_particle != nullptr)
		_end_particle = nullptr;
}

vector<shared_ptr<Particle>> GridCell::GetNeighbourParticles()
{
	vector<shared_ptr<Particle>> neighbours;
	//vector<shared_ptr<GridCell>> neighbour_cells = _parent->GetNeighbouringCells(_i, _j, _k);

	for (int i = 0; i < _neighbour_cells.size(); ++i)
	{
		shared_ptr<GridCell> &cell = _neighbour_cells[i];
		shared_ptr<Particle> &root = cell->GetRootParticle();

		if (root != nullptr)
		{
			neighbours.push_back(root);
		}

		/*vector<shared_ptr<Particle>>& particles = cell->GetParticles();
		for (int j = 0; j < particles.size(); ++j)
		{
			neighbours.push_back(particles[j]);
		}*/
	}

	return neighbours;
}

vector<shared_ptr<Particle>> GridCell::GetBoundaryParticles()
{
	vector<shared_ptr<Particle>> boundary_neighbours;
	//vector<shared_ptr<GridCell>> neighbour_cells = _parent->GetNeighbouringCells(_i, _j, _k);

	for (int i = 0; i < _neighbour_cells.size(); ++i)
	{
		shared_ptr<GridCell> &cell = _neighbour_cells[i];
		shared_ptr<Particle> &root = cell->GetRootBoundary();

		if (root != nullptr)
		{
			boundary_neighbours.push_back(root);
		}
	}
	return boundary_neighbours;
}


vector<IndexRange> GridCell::GetNeighbourIndices()
{
	vector<IndexRange> neighbours;
	//vector<shared_ptr<GridCell>> neighbour_cells = _parent->GetNeighbouringCells(_i, _j, _k);

	if (_range.start > -1 && _range.end >= -1)
		neighbours.push_back(_range);

	for (int i = 0; i < _neighbour_cells.size(); ++i)
	{
		shared_ptr<GridCell> &cell = _neighbour_cells[i];
		/*shared_ptr<Particle> &root = cell->GetRootParticle();

		if (root != nullptr)
		{
		neighbours.push_back(root);
		}*/

		IndexRange range = cell->GetIndexRange();
		if (range.start > -1 && range.end > -1)
		{
			neighbours.push_back(range);
		}

		//vector<uint64_t> indices = cell->GetParticleIndices();
		/*for (int j = 0; j < indices.size(); ++j)
		{
			neighbours.push_back(indices[j]);
		}*/
	}

	return neighbours;
}


void GridCell::CalculateNeighbourBoundaryIndices()
{
	if (_boundary_range.start > -1 && _boundary_range.end >= -1)
		_boundary_neighbours.push_back(_boundary_range);

	for (int i = 0; i < _neighbour_cells.size(); ++i)
	{
		shared_ptr<GridCell> &cell = _neighbour_cells[i];
		
		IndexRange range = cell->GetBoundaryIndexRange();
		if (range.start > -1 && range.end > -1)
		{
			_boundary_neighbours.push_back(range);
		}
	}
}


//vector<uint64_t> GridCell::GetNeighbourIndices()
//{
//	vector<uint64_t> neighbours;
//	//vector<shared_ptr<GridCell>> neighbour_cells = _parent->GetNeighbouringCells(_i, _j, _k);
//
//	for (int i = 0; i < _neighbour_cells.size(); ++i)
//	{
//		shared_ptr<GridCell> &cell = _neighbour_cells[i];
//		/*shared_ptr<Particle> &root = cell->GetRootParticle();
//
//		if (root != nullptr)
//		{
//		neighbours.push_back(root);
//		}*/
//
//		vector<uint64_t> indices = cell->GetParticleIndices();
//		for (int j = 0; j < indices.size(); ++j)
//		{
//			neighbours.push_back(indices[j]);
//		}
//	}
//
//	return neighbours;
//}

vector<uint64_t> GridCell::GetParticleIndices()
{
	vector<uint64_t> indices;
	if (_particles.size() > 0)
	{
		for (int i = 0; i < _particles.size(); ++i)
		{
			indices.push_back(_particles[i]->GetIndex());
		}
	}
	return indices;
}

vector<vector<shared_ptr<Particle>>> GridCell::GetNeighbourParticleList()
{
	vector<vector<shared_ptr<Particle>>> neighbours;
	//vector<shared_ptr<GridCell>> neighbour_cells = _parent->GetNeighbouringCells(_i, _j, _k);

	for (int i = 0; i < _neighbour_cells.size(); ++i)
	{
		shared_ptr<GridCell> &cell = _neighbour_cells[i];
		//vector<shared_ptr<Particle>> &particles = 
		neighbours.push_back(cell->GetParticles());
	}
	return neighbours;
}

void GridCell::CalculateZIndex()
{
	unsigned int x = (unsigned int)_i;
	unsigned int y = (unsigned int)_j;
	unsigned int z = (unsigned int)_k;

	uint64_t answer = 0;
	for (uint64_t i = 0; i < (sizeof(uint64_t)* CHAR_BIT) / 3; ++i) {
		answer |= ((x & ((uint64_t)1 << i)) << 2 * i) | ((y & ((uint64_t)1 << i)) << (2 * i + 1)) | ((z & ((uint64_t)1 << i)) << (2 * i + 2));
	}
	_index = answer;
}


void GridCell::CleanUp()
{
	for (int i = 0; i < _neighbour_cells.size(); ++i)
	{
		_neighbour_cells[i] = nullptr;
	}
	_neighbour_cells.clear();
	_parent = nullptr;
}