#ifndef IGRID_H
#define IGRID_H

#include "Vector3D.h"
#include <memory>

using namespace std;

class Particle;

class IGrid
{
public:
	virtual void ClearParticles() = 0;
	virtual void AddParticle(shared_ptr<Particle>& p) = 0;

	virtual Vector3D GetSize() = 0;
	virtual Vector3D GetPosition() = 0;
	virtual float GetCellSize() = 0;

	virtual Int3 GetGridPosition(Vector3D position) = 0;
	
	virtual void Reset() = 0;
};

#endif