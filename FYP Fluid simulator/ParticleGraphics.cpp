#include "ParticleGraphics.h"



HRESULT ParticleGraphics::InitGraphics(HWND& hWnd, HINSTANCE hInst)
{
	HRESULT hr = S_OK;

	RECT rc;
	GetClientRect(hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	UINT createDeviceFlags = 0;
	#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
	#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = width;
	sd.BufferDesc.Height = height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		_driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(NULL, _driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &_swapChain, &_device, &_featureLevel, &_deviceContext);
		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
		return hr;
	
	// Create a render target view
	ID3D11Texture2D* pBackBuffer = NULL;
	hr = _swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(hr))
		return hr;

	hr = _device->CreateRenderTargetView(pBackBuffer, NULL, &_renderTargetView);
	pBackBuffer->Release();
	if (FAILED(hr))
		return hr;
	

	// Setup the viewport
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)width;
	vp.Height = (FLOAT)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	_deviceContext->RSSetViewports(1, &vp);

	D3D11_BUFFER_DESC bd;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = _device->CreateBuffer(&bd, nullptr, &_constantBuffer);
	
	
	if (FAILED(hr))
		return hr;


	// Set primitive topology
	_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	D3D11_RASTERIZER_DESC rd;
	ZeroMemory(&rd, sizeof(D3D11_RASTERIZER_DESC));

	rd.FillMode = D3D11_FILL_SOLID;
	rd.CullMode = D3D11_CULL_BACK;
	rd.FrontCounterClockwise = false;
	rd.DepthClipEnable = true;

	hr = _device->CreateRasterizerState(&rd, &_rasterizerState);
	if (FAILED(hr))
		return hr;

	//rd.FillMode = D3D11_FILL_WIREFRAME;
	//rd.CullMode = D3D11_CULL_NONE;

	hr = _device->CreateRasterizerState(&rd, &_wireframeRasterizer);
	if (FAILED(hr))
		return hr;

	_deviceContext->RSSetState(_rasterizerState);

	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	ZeroMemory(&rtbd, sizeof(rtbd));

	rtbd.BlendEnable = true;
	rtbd.SrcBlend = D3D11_BLEND_SRC_COLOR;
	rtbd.DestBlend = D3D11_BLEND_BLEND_FACTOR;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ONE;
	rtbd.DestBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = D3D10_COLOR_WRITE_ENABLE_ALL;

	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.RenderTarget[0] = rtbd;

	//_device->CreateBlendState(&blendDesc, &Transparency);

	//Describe our Depth/Stencil Buffer
	D3D11_TEXTURE2D_DESC depthStencilDesc;

	depthStencilDesc.Width = width;
	depthStencilDesc.Height = height;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	_device->CreateTexture2D(&depthStencilDesc, NULL, &_depthStencilBuffer);
	_device->CreateDepthStencilView(_depthStencilBuffer, NULL, &_depthStencilView);

	_deviceContext->OMSetRenderTargets(1, &_renderTargetView, _depthStencilView);

	ID3DBlob* pVSBlob = NULL;
	hr = CompileShaderFromFile(L"ParticleShader.fx", "VS", "vs_4_0", &pVSBlob);
	if (FAILED(hr))
	{
		ErrorMessage();
		return hr;
	}

	hr = _device->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &_vertexShader);

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		//{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	// Create the input layout
	hr = _device->CreateInputLayout(layout, ARRAYSIZE(layout), pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &_inputLayout);

	if (FAILED(hr))
		return hr;

	pVSBlob->Release();

	ID3DBlob* pPSBlob = NULL;
	hr = CompileShaderFromFile(L"ParticleShader.fx", "PS", "ps_4_0", &pPSBlob);
	if (FAILED(hr))
	{
		ErrorMessage();
		return hr;
	}

	hr = _device->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &_pixelShader);

	if (FAILED(hr))
		return hr;

	pPSBlob->Release();

	hr = InitVertexData();

	if (FAILED(hr))
		return hr;

	hr = InitCubeData();

	if (FAILED(hr))
		return hr;

	XMVECTOR Eye = XMVectorSet(0.0f, 0.0f, -0.5f, 1.0f);
	XMVECTOR At = XMVectorSet(0.0f, 0.1f, 0.0f, 1.0f);
	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMStoreFloat4(&_cam.Eye, Eye);
	XMStoreFloat4(&_cam.At, At);
	XMStoreFloat4(&_cam.Up, Up);

	_cam.OriginalPos = _cam.Eye;

	XMMATRIX view = XMMatrixLookAtLH(Eye, At, Up);
	XMStoreFloat4x4(&_view, view);

	XMMATRIX projection = XMMatrixPerspectiveFovLH(0.35f * XM_PI, (float)width / (float)height,
		0.001f, 25.0f);
	XMStoreFloat4x4(&_projection, projection);

	_lightDir = XMFLOAT4(1.5f, 1.5f, -0.5f, 1.0f);

	_input = new directInput();
	_input->initDirectInput(hInst, hWnd);

	return S_OK;
}

void ParticleGraphics::Prepare()
{
	float ClearColor[4] = { 0.5f, 0.5f, 0.65f, 1.0f }; // red,green,blue,alpha
	_deviceContext->ClearRenderTargetView(_renderTargetView, ClearColor);
	_deviceContext->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	// Handle input
	_input->updateMouseInput();
	if (_input->getRightClick())
	{
		float camChangeX = _input->getXChange() * XM_PI / 500.0f;
		float camChangeY = _input->getYChange() * XM_PI / 500.0f;

		_cam.rotation[1] += camChangeX;
		_cam.rotation[0] += camChangeY;

		if (_cam.rotation[0] > XM_PI * 3 / 8)
		{
			_cam.rotation[0] = XM_PI * 3 / 8;
		}
		else if (_cam.rotation[0] < -XM_PI * 3 / 8)
		{
			_cam.rotation[0] = -XM_PI * 3 / 8;
		}

		if (_cam.rotation[1] > 2 * XM_PI)
		{
			_cam.rotation[1] -= (2 * XM_PI);
		}
		else if (_cam.rotation[1] > 0)
		{
			_cam.rotation[1] += (2 * XM_PI);
		}

		XMMATRIX rotationMatrix = XMMatrixRotationX(_cam.rotation[0]) * XMMatrixRotationY(_cam.rotation[1]);
		XMVECTOR pos = XMVector3Transform(XMLoadFloat4(&_cam.OriginalPos), rotationMatrix);
		XMStoreFloat4(&_cam.Eye, pos);
		XMStoreFloat4x4(&_view, XMMatrixLookAtLH(XMLoadFloat4(&_cam.Eye), XMLoadFloat4(&_cam.At), XMLoadFloat4(&_cam.Up)));
	}

	_deviceContext->IASetInputLayout(_inputLayout);
	
	_deviceContext->VSSetShader(_vertexShader, nullptr, 0);
	_deviceContext->PSSetShader(_pixelShader, nullptr, 0);

	_deviceContext->VSSetConstantBuffers(0, 1, &_constantBuffer);
	_deviceContext->PSSetConstantBuffers(0, 1, &_constantBuffer);
}

void ParticleGraphics::DrawParticles(vector<shared_ptr<Particle>> particles, float h)
{
	XMMATRIX view = XMLoadFloat4x4(&_view);
	XMMATRIX projection = XMLoadFloat4x4(&_projection);
	
	ConstantBuffer cb;

	cb.view = XMMatrixTranspose(view);
	cb.projection = XMMatrixTranspose(projection);
	cb.lightDir = _lightDir;
	cb.colour = XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	_deviceContext->IASetVertexBuffers(0, 1, &_vertexBuffer, &stride, &offset);
	_deviceContext->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R16_UINT, 0);

	//for_each(particles.begin(), particles.end(), DrawParticle);

	float particle_size = h / 2.0f;
	
	for (auto &particle : particles)
	{
		Vector3D pos = particle->GetPosition();

		XMMATRIX world = XMMatrixScaling(particle_size, particle_size, particle_size) * XMMatrixTranslation(pos.x, pos.y, pos.z);
		cb.world = XMMatrixTranspose(world);

		//cb.colour = XMFLOAT4((particle->GetPressure() + 2000.0f / 4000.0f), 0.0f, 0.0f, 1.0f);
		cb.colour = particle->GetColour();
		//cb.colour = XMFLOAT4(0.0f, (particle->GetPressure()  / 1000.0f), 0.0f, 1.0f);

		// Update constant buffer
		_deviceContext->UpdateSubresource(_constantBuffer, 0, nullptr, &cb, 0, 0);

		// Draw object
		_deviceContext->DrawIndexed(index_count, 0, 0);
		//_deviceContext->Draw(3, 0);
	}	
}

void ParticleGraphics::DrawGrid(Vector3D position, Vector3D bounds)
{
	bounds = 0.5f * bounds;

	_deviceContext->RSSetState(_wireframeRasterizer);

	XMMATRIX view = XMLoadFloat4x4(&_view);
	XMMATRIX projection = XMLoadFloat4x4(&_projection);

	ConstantBuffer cb;

	cb.view = XMMatrixTranspose(view);
	cb.projection = XMMatrixTranspose(projection);
	cb.lightDir = _lightDir;
	cb.colour = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	XMMATRIX world = XMMatrixTranslation(1.0f, 1.0f, 1.0f) * XMMatrixScaling(bounds.x, bounds.y, bounds.z) * XMMatrixTranslation(position.x, position.y, position.z);

	cb.world = XMMatrixTranspose(world);
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	_deviceContext->IASetVertexBuffers(0, 1, &_cubeVertexBuffer, &stride, &offset);
	_deviceContext->IASetIndexBuffer(_cubeIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	
	_deviceContext->UpdateSubresource(_constantBuffer, 0, nullptr, &cb, 0, 0);

	_deviceContext->DrawIndexed(cube_index_count, 0, 0);
}

void ParticleGraphics::Draw()
{
	_swapChain->Present(0, 0);
}

void ParticleGraphics::DrawParticle(shared_ptr<Particle> particle)
{
	/*Vector3D pos = particle->GetPosition();

	XMMATRIX world = XMMatrixTranslation(pos.x, pos.y, pos.z);
	cb.world = XMMatrixTranspose(world);*/

	// Update constant buffer
	//_deviceContext->UpdateSubresource(_constantBuffer, 0, nullptr, &cb, 0, 0);

	// Draw object
	_deviceContext->DrawIndexed(index_count, 0, 0);
}

void ParticleGraphics::CleanUp()
{
	_input->cleanUpInput();

	if (_device) _device->Release();
	if (_deviceContext) _deviceContext->Release();
	if (_swapChain) _swapChain->Release();
	if (_renderTargetView) _renderTargetView->Release();
	if (_vertexShader) _vertexShader->Release();
	if (_pixelShader) _pixelShader->Release();
	if (_inputLayout) _inputLayout->Release();
	if (_rasterizerState) _rasterizerState->Release();
	if (_wireframeRasterizer) _wireframeRasterizer->Release();
	if (_constantBuffer) _constantBuffer->Release();
	if (_vertexBuffer) _vertexBuffer->Release();
	if (_indexBuffer) _indexBuffer->Release();
	if (_cubeVertexBuffer) _cubeVertexBuffer->Release();
	if (_cubeIndexBuffer) _cubeIndexBuffer->Release();
	if (_depthStencilView) _depthStencilView->Release();
	if (_depthStencilBuffer) _depthStencilBuffer->Release();
}

HRESULT ParticleGraphics::InitCubeData()
{
	/*vector<Vertex> vertices;
	vector<WORD> indices;*/
	int vertex_count = 24;
	cube_index_count = 36;

	Vertex vertices[] =
	{
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(-1.0f, 1.0f, -1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(1.0f, 1.0f, -1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(-1.0f, 1.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f, -1.0f, -1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(1.0f, -1.0f, -1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(1.0f, -1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(-1.0f, -1.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(-1.0f, -1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f, -1.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(-1.0f, 1.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(-1.0f, 1.0f, 1.0f) },

		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(1.0f, -1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(1.0f, -1.0f, -1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(1.0f, 1.0f, -1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f, -1.0f, -1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(1.0f, -1.0f, -1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(1.0f, 1.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(-1.0f, 1.0f, -1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(-1.0f, -1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(1.0f, -1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(-1.0f, 1.0f, 1.0f) },
	};

	HRESULT hr;

	D3D11_BUFFER_DESC bd;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(Vertex) * vertex_count;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices;
	hr = _device->CreateBuffer(&bd, &InitData, &_cubeVertexBuffer);

	WORD indices[] =
	{
		3, 1, 0,
		2, 1, 3,

		6, 4, 5,
		7, 4, 6,

		11, 9, 8,
		10, 9, 11,

		14, 12, 13,
		15, 12, 14,

		19, 17, 16,
		18, 17, 19,

		22, 20, 21,
		23, 20, 22
	};

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(WORD) * cube_index_count;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &indices;

	hr = _device->CreateBuffer(&bd, &InitData, &_cubeIndexBuffer);

	if (FAILED(hr))
		return hr;

	return S_OK;
}

HRESULT ParticleGraphics::InitVertexData()
{
	vector<Vertex> vertices;
	vector<WORD> indices;
	int vertex_count = 0;
	index_count = 0;

	int detail = 25;
	float radius = 1.0f;
	float angleX; float angleY;
	float step = 2 * XM_PI / (float)detail;
	XMFLOAT4 colour = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);

	/*vertices.push_back({ XMFLOAT3(0.0f, 0.5f, 0.5f) });
	vertices.push_back({ XMFLOAT3(0.5f, -0.5f, 0.5f) });
	vertices.push_back({ XMFLOAT3(-0.5f, -0.5f, 0.5f) });*/

	// Create vertex buffer
	/*Vertex vertices[] =
	{
		XMFLOAT3(0.0f, 0.5f, 0.5f),
		XMFLOAT3(0.5f, -0.5f, 0.5f),
		XMFLOAT3(-0.5f, -0.5f, 0.5f),
	};*/

	/*vertex_count = 3;

	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	index_count = 3;*/

	vertices.push_back({ XMFLOAT3(0.0f, radius, 0.0f), XMFLOAT3(0.0f, radius, 0.0f) });

	for (int i = 1; i < detail - 1; ++i)
	{
		angleX = step * i / 2.0f;
		for (int j = 0; j < detail; ++j)
		{
			angleY = XM_PI * 2 - (step * j);
			XMFLOAT3 next = SphereValue(angleX, angleY, radius);
			vertices.push_back({ next, next });
			vertex_count++;

			angleY -= step;
		}
	}

	vertices.push_back({ XMFLOAT3(0.0f, -radius, 0.0f), XMFLOAT3(0.0f, -radius, 0.0f) });

	for (int i = 1; i < detail + 1; ++i)
	{
		indices.push_back(i);
		indices.push_back(0);
		indices.push_back(i + 1);
		index_count += 3;
	}

	for (int i = 1; i < vertex_count - detail - 1; ++i)
	{
		indices.push_back(i);
		indices.push_back(i + 1);
		indices.push_back(i + 1 + detail);
		indices.push_back(i);
		indices.push_back(i + 1 + detail);
		indices.push_back(i + detail);
		index_count += 6;
	}

	for (int i = vertex_count - 1; i > vertex_count - detail - 1; i--)
	{
		indices.push_back(i);
		indices.push_back(vertex_count - 1);
		indices.push_back(i - 1);
		index_count += 3;
	}

	HRESULT hr;

	D3D11_BUFFER_DESC bd;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(Vertex) * vertex_count;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices[0];
	hr = _device->CreateBuffer(&bd, &InitData, &_vertexBuffer);

	if (FAILED(hr))
		return hr;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(WORD) * index_count;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &indices[0];

	hr = _device->CreateBuffer(&bd, &InitData, &_indexBuffer);

	if (FAILED(hr))
		return hr;

	return S_OK;
}