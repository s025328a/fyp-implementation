#include "Density.h"

float Density::Calculate(shared_ptr<Particle>& i, shared_ptr<Particle>& j)
{
	//Vector3D position = particle->GetPosition();
	
	float density = _kernel->Value(i->GetPosition() - j->GetPosition(), core) * i->GetMass();
	
	i->AddDensity(density);

	return density;
}

Density::Density(ISmoothingKernel* kernel)
{
	_kernel = kernel;
}